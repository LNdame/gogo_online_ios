import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:gogo_online_ios/route_constants.dart';
import 'package:gogo_online_ios/route_generator.dart';
import 'package:gogo_online_ios/src/controllers/app_controller.dart';
import 'package:gogo_online_ios/src/controllers/splash_screen_controller.dart';
import 'package:gogo_online_ios/src/controllers/user_controller.dart';
import 'package:gogo_online_ios/src/models/setting.dart';
import 'package:gogo_online_ios/src/repository/settings_repository.dart' as setting_repo;
import 'package:mvc_pattern/mvc_pattern.dart';
import 'src/helpers/app_config.dart' as config;
import 'package:gogo_online_ios/src/helpers/custom_trace.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  }
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await GlobalConfiguration().loadFromAsset("configurations");
  if (kDebugMode) {
    print(CustomTrace(StackTrace.current, message: "base_url: ${GlobalConfiguration().getValue('base_url')}"));
    print(CustomTrace(StackTrace.current, message: "api_base_url: ${GlobalConfiguration().getValue('api_base_url')}"));
  }
  HttpOverrides.global =  MyHttpOverrides();
  runApp(const MyApp());
}

class MyApp extends AppStatefulWidgetMVC {
  const MyApp({Key? key}) : super(key: key);

  @override
  AppStateMVC <MyApp> createState() => _MyAppState();
}

class _MyAppState extends AppStateMVC<MyApp> {
  factory _MyAppState() => _this ??= _MyAppState._();
  static _MyAppState? _this;

  _MyAppState._():super(controller: AppController(), controllers: [
    SplashScreenController(),UserController(),
  ]);

  Widget buildApp(BuildContext context) {
    return  ValueListenableBuilder(valueListenable: setting_repo.setting,
        builder: (context, Setting setgs, _){
          return ScreenUtilInit(
            designSize: const Size(360, 690),
              builder: (context, child)=> MaterialApp(
                navigatorKey: setting_repo.navigatorKey,
                title: setgs.appName,
                initialRoute: splashRoute,
                onGenerateRoute: RouteGenerator.generateRoute,
                locale: setgs.mobileLanguage.value,
                localizationsDelegates: const [
                  AppLocalizations.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                ],
                supportedLocales: AppLocalizations.supportedLocales,
                theme: setgs.brightness.value == Brightness.light
                    ? ThemeData(
                  fontFamily: 'ProductSans',
                  primaryColor: Colors.white,
                  floatingActionButtonTheme: FloatingActionButtonThemeData(elevation: 0, foregroundColor: Colors.white),
                  brightness: Brightness.light,
                  dividerColor: config.Colors().accentColor(0.1),
                  focusColor: config.Colors().accentColor(1),
                  hintColor: config.Colors().secondColor(1),
                  textTheme: TextTheme(
                    headlineSmall: TextStyle(fontSize: 22.0, color: config.Colors().secondColor(1), height: 1.3),
                    headlineMedium: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w700, color: config.Colors().secondColor(1), height: 1.3),
                    displaySmall: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700, color: config.Colors().secondColor(1), height: 1.3),
                    displayMedium: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w700, color: config.Colors().mainColor(1), height: 1.4),
                    displayLarge: TextStyle(fontSize: 26.0, fontWeight: FontWeight.w300, color: config.Colors().secondColor(1), height: 1.4),
                    titleMedium: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500, color: config.Colors().secondColor(1), height: 1.3),
                    titleLarge: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w700, color: config.Colors().mainColor(1), height: 1.3),
                    bodyMedium: TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400, color: config.Colors().secondColor(1), height: 1.2),
                    bodyLarge: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400, color: config.Colors().secondColor(1), height: 1.3),
                    bodySmall: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w300, color: config.Colors().accentColor(1), height: 1.2),
                  ),
                  colorScheme: ColorScheme.fromSwatch().copyWith(secondary: config.Colors().mainColor(1), brightness: Brightness.light, ),

                )
                    : ThemeData(
                  fontFamily: 'ProductSans',
                  primaryColor: Color(0xFF252525),
                  brightness: Brightness.dark,
                  scaffoldBackgroundColor: Color(0xFF2C2C2C),
                  dividerColor: config.Colors().accentColor(0.1),
                  hintColor: config.Colors().secondDarkColor(1),
                  focusColor: config.Colors().accentDarkColor(1),
                  textTheme: TextTheme(
                    headlineSmall: TextStyle(fontSize: 22.0, color: config.Colors().secondDarkColor(1), height: 1.3),
                    headlineMedium: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w700, color: config.Colors().secondDarkColor(1), height: 1.3),
                    displaySmall: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700, color: config.Colors().secondDarkColor(1), height: 1.3),
                    displayMedium: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w700, color: config.Colors().mainDarkColor(1), height: 1.4),
                    displayLarge: TextStyle(fontSize: 26.0, fontWeight: FontWeight.w300, color: config.Colors().secondDarkColor(1), height: 1.4),
                    titleMedium: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500, color: config.Colors().secondDarkColor(1), height: 1.3),
                    titleLarge: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w700, color: config.Colors().mainDarkColor(1), height: 1.3),
                    bodyMedium: TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400, color: config.Colors().secondDarkColor(1), height: 1.2),
                    bodyLarge: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400, color: config.Colors().secondDarkColor(1), height: 1.3),
                    bodySmall: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w300, color: config.Colors().secondDarkColor(0.6), height: 1.2),
                  ),
                  colorScheme: ColorScheme.fromSwatch().copyWith(secondary: config.Colors().mainDarkColor(1), brightness: Brightness.dark,),
                ),
               // home: const MyHomePage(title: 'Flutter Demo Home Page'),
              ),
          );
        }
        );
  }

  @override
  Widget buildChild(BuildContext context) {
    return  ValueListenableBuilder(valueListenable: setting_repo.setting,
        builder: (context, Setting setgs, _){
          return ScreenUtilInit(
            designSize: const Size(360, 690),
            builder: (context, child)=> MaterialApp(
              navigatorKey: setting_repo.navigatorKey,
              title: setgs.appName,
              initialRoute: splashRoute,
              onGenerateRoute: RouteGenerator.generateRoute,
              locale: setgs.mobileLanguage.value,
              localizationsDelegates: const [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: AppLocalizations.supportedLocales,
              theme: setgs.brightness.value == Brightness.light
                  ? ThemeData(
                fontFamily: 'ProductSans',
                primaryColor: Colors.white,
                floatingActionButtonTheme: FloatingActionButtonThemeData(elevation: 0, foregroundColor: Colors.white),
                brightness: Brightness.light,
                dividerColor: config.Colors().accentColor(0.1),
                focusColor: config.Colors().accentColor(1),
                hintColor: config.Colors().secondColor(1),
                textTheme: TextTheme(
                  headlineSmall: TextStyle(fontSize: 22.0, color: config.Colors().secondColor(1), height: 1.3),
                  headlineMedium: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w700, color: config.Colors().secondColor(1), height: 1.3),
                  displaySmall: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700, color: config.Colors().secondColor(1), height: 1.3),
                  displayMedium: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w700, color: config.Colors().mainColor(1), height: 1.4),
                  displayLarge: TextStyle(fontSize: 26.0, fontWeight: FontWeight.w300, color: config.Colors().secondColor(1), height: 1.4),
                  titleMedium: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500, color: config.Colors().secondColor(1), height: 1.3),
                  titleLarge: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w700, color: config.Colors().mainColor(1), height: 1.3),
                  bodyMedium: TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400, color: config.Colors().secondColor(1), height: 1.2),
                  bodyLarge: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400, color: config.Colors().secondColor(1), height: 1.3),
                  bodySmall: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w300, color: config.Colors().accentColor(1), height: 1.2),
                ),
                colorScheme: ColorScheme.fromSwatch().copyWith(secondary: config.Colors().mainColor(1), brightness: Brightness.light, ),

              )
                  : ThemeData(
                fontFamily: 'ProductSans',
                primaryColor: Color(0xFF252525),
                brightness: Brightness.dark,
                scaffoldBackgroundColor: Color(0xFF2C2C2C),
                dividerColor: config.Colors().accentColor(0.1),
                hintColor: config.Colors().secondDarkColor(1),
                focusColor: config.Colors().accentDarkColor(1),
                textTheme: TextTheme(
                  headlineSmall: TextStyle(fontSize: 22.0, color: config.Colors().secondDarkColor(1), height: 1.3),
                  headlineMedium: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w700, color: config.Colors().secondDarkColor(1), height: 1.3),
                  displaySmall: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700, color: config.Colors().secondDarkColor(1), height: 1.3),
                  displayMedium: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w700, color: config.Colors().mainDarkColor(1), height: 1.4),
                  displayLarge: TextStyle(fontSize: 26.0, fontWeight: FontWeight.w300, color: config.Colors().secondDarkColor(1), height: 1.4),
                  titleMedium: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500, color: config.Colors().secondDarkColor(1), height: 1.3),
                  titleLarge: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w700, color: config.Colors().mainDarkColor(1), height: 1.3),
                  bodyMedium: TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400, color: config.Colors().secondDarkColor(1), height: 1.2),
                  bodyLarge: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400, color: config.Colors().secondDarkColor(1), height: 1.3),
                  bodySmall: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w300, color: config.Colors().secondDarkColor(0.6), height: 1.2),
                ),
                colorScheme: ColorScheme.fromSwatch().copyWith(secondary: config.Colors().mainDarkColor(1), brightness: Brightness.dark,),
              ),
              // home: const MyHomePage(title: 'Flutter Demo Home Page'),
            ),
          );
        }
    );
  }

  
  // This widget is the root of your application.
}


