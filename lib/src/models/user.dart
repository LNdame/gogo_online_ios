import 'role.dart';
import '../models/media.dart';

class User {
    String? id;
    String? name;
    String? email;
    String? password;
    String? apiToken;
    String? deviceToken;
    String? phone;
    String? address;
    String? bio;
    Media? image;
  late  Role? role;

  // used for indicate if client logged in or not
  late  bool? auth ;
  late  String? firebaseUid;

  User(
      {this.id,
      this.name,
      this.email,
      this.password,
      this.apiToken,
      this.deviceToken,
      this.phone,
      this.address,
      this.bio,
      this.image,
      this.role,
      this.auth,
      this.firebaseUid});

  User.fromJSON(Map<String, dynamic> jsonMap,
      {this.id,
      this.name,
      this.email,
      this.password,
      this.apiToken,
      this.deviceToken,
      this.phone,
      this.address,
      this.bio,
      this.image,
      this.role,
      this.auth,
      this.firebaseUid}) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'] ?? '';
      email = jsonMap['email'] ?? '';
      firebaseUid = jsonMap['firebase_uid'] ?? '';
      apiToken = jsonMap['api_token'];
      deviceToken = jsonMap['device_token'];
      try {
        phone = jsonMap['custom_fields']['phone']['view'];
      } catch (e) {
        phone = "";
      }
      try {
        address = jsonMap['custom_fields']['address']['view'];
      } catch (e) {
        address = "";
      }
      try {
        bio = jsonMap['custom_fields']['bio']['view'];
      } catch (e) {
        bio = "";
      }
      image = jsonMap['media'] != null && (jsonMap['media'] as List).isNotEmpty
          ? Media.fromJSON(jsonMap['media'][0])
          :  Media();
      role = jsonMap['roles'] != null && (jsonMap['roles'] as List).isNotEmpty
          ? Role.fromJSON(jsonMap['roles'][0])
          :  Role();
    } catch (e) {
      print(e);
    }
  }

  Map toMap() {
    var map =  <String, dynamic>{};
    map["id"] = id;
    map["email"] = email;
    map["firebase_uid"] = firebaseUid;
    map["name"] = name;
    map["password"] = password;
    map["api_token"] = apiToken;
    if (deviceToken != null) {
      map["device_token"] = deviceToken;
    }
    map["phone"] = phone;
    map["address"] = address;
    map["bio"] = bio;
    map["media"] = image?.toMap();
    map['roles'] = role?.toMap();
    return map;
  }

  @override
  String toString() {
    var map = toMap();
    map["auth"] = auth;
    return map.toString();
  }

  bool profileCompleted() {
    return phone != null && phone != '';
  }
}
