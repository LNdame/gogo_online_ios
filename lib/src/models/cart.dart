import '../helpers/custom_trace.dart';
import '../models/option.dart';
import '../models/product.dart';

class Cart {
 late String id;
 late Product product;
 late  double quantity;
 late  List<Option> options;
 late  String userId;
 late  String consultationStartTime;
 late  String consultationEndTime;
 late String consultationDate;

  Cart(
      );

  Cart.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      quantity = jsonMap['quantity'] != null ? jsonMap['quantity'].toDouble() : 0.0;
      product = jsonMap['product'] != null ? Product.fromJSON(jsonMap['product']) : Product.fromJSON({});
      options = jsonMap['options'] != null ? List.from(jsonMap['options']).map((element) => Option.fromJSON(element)).toList() : [];
      consultationStartTime = jsonMap['consultation_start_time'] != null ? jsonMap['consultation_start_time'].toString() : '';
      consultationEndTime = jsonMap['consultation_end_time'] != null ? jsonMap['consultation_end_time'].toString() : '';
      consultationDate = jsonMap['consultation_date'] != null ? jsonMap['consultation_date'].toString() : '';
    } catch (e) {
      id = '';
      quantity = 0.0;
      product = Product.fromJSON({});
      options = [];
      consultationStartTime ='';
      consultationEndTime='';
      consultationDate='';
      print(CustomTrace(StackTrace.current, message: e.toString()));
    }
  }

  Map toMap() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["quantity"] = quantity;
    map["product_id"] = product.id;
    map["user_id"] = userId;
    map["options"] = options.map((element) => element.id).toList();
    map["consultation_start_time"] = consultationStartTime;
    map["consultation_end_time"] = consultationEndTime;
    map["consultation_date"] = consultationDate;
    return map;
  }

  double getProductPrice() {
    double result = product.price;
    if (options.isNotEmpty) {
      for (var option in options) {
        result += option.price != null ? option.price : 0;
      }
    }
    return result;
  }

  bool isSame(Cart cart) {
    bool _same = true;
    _same &= this.product == cart.product;
    _same &= this.options.length == cart.options.length;
    if (_same) {
      options.forEach((Option _option) {
        _same &= cart.options.contains(_option);
      });
    }
    return _same;
  }

  @override
  bool operator ==(dynamic other) {
    return other.id == id;
  }

  @override
  int get hashCode => id.hashCode;
}
