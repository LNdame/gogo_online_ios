import '../models/media.dart';

class Field {
  late String id;
  late String name;
  late String description;
  late  Media image;
  late bool selected;

  Field();

  Field.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'];
      description = jsonMap['description'];
      image = jsonMap['media'] != null && (jsonMap['media'] as List).isNotEmpty ? Media.fromJSON(jsonMap['media'][0]) : Media();
      selected = jsonMap['selected'] ?? false;
    } catch (e) {
      id = '';
      name = '';
      description = '';
      image = Media();
      selected = false;
      print(e);
    }
  }

  Map<String, dynamic> toMap() {
    var map =  <String, dynamic>{};
    map['id'] = id;
    return map;
  }

  @override
  bool operator ==(dynamic other) {
    return other.id == this.id;
  }

  @override
  int get hashCode => super.hashCode;
}
