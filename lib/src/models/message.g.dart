// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Message _$MessageFromJson(Map<String, dynamic> json) => Message(
      content: json['content'] as String?,
      fromId: json['fromId'] as String?,
      toId: json['toId'] as String?,
      timeStamp: json['timeStamp'] as String?,
      sendDate: json['sendDate'] == null
          ? null
          : DateTime.parse(json['sendDate'] as String),
      isSeen: json['isSeen'] as bool?,
      type: $enumDecodeNullable(_$MessageTypeEnumMap, json['type']),
      mediaType: $enumDecodeNullable(_$MediaTypeEnumMap, json['mediaType']),
      mediaUrl: json['mediaUrl'] as String?,
      uploadFinished: json['uploadFinished'] as bool?,
      reply: json['reply'] == null
          ? null
          : ReplyMessage.fromJson(json['reply'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'content': instance.content,
      'fromId': instance.fromId,
      'toId': instance.toId,
      'timeStamp': instance.timeStamp,
      'sendDate': instance.sendDate?.toIso8601String(),
      'isSeen': instance.isSeen,
      'type': _$MessageTypeEnumMap[instance.type],
      'mediaType': _$MediaTypeEnumMap[instance.mediaType],
      'mediaUrl': instance.mediaUrl,
      'uploadFinished': instance.uploadFinished,
      'reply': instance.reply?.toJson(),
    };

const _$MessageTypeEnumMap = {
  MessageType.Text: 'Text',
  MessageType.Media: 'Media',
};

const _$MediaTypeEnumMap = {
  MediaType.Photo: 'Photo',
  MediaType.Video: 'Video',
};
