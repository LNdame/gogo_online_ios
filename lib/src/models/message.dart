import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gogo_online_ios/src/helpers/app_constants.dart';
import 'package:gogo_online_ios/src/models/reply_message.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'message.g.dart';

@JsonSerializable(explicitToJson: true)
class Message {
  late String? content;
  late String? fromId;
  late String? toId;
  late String? timeStamp;
  late DateTime? sendDate;
  late bool? isSeen;
  late MessageType? type;
  late MediaType? mediaType;
  late String? mediaUrl;
  late bool? uploadFinished;
  late ReplyMessage? reply;

  Message({
    this.content,
    this.fromId,
    this.toId,
    this.timeStamp,
    this.sendDate,
    this.isSeen,
    this.type,
    this.mediaType,
    this.mediaUrl,
    this.uploadFinished,
    this.reply,
  });

  factory Message.fromMap(Map<String, dynamic> data) {
    return _$MessageFromJson(data);
  }

  factory Message.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) {
    final data = snapshot.data();
    return Message(
      content: data?["content"],
      fromId: data?["fromId"],
      toId: data?["toId"],
      timeStamp: data?["timeStamp"],
      sendDate: data?["sendDate"],
      isSeen: data?["isSeen"],
      type: data?["type"],
      mediaType: data?["mediaType"],
      mediaUrl: data?["mediaUrl"],
      uploadFinished: data?["uploadFinished"],
      reply: data?["reply"],
    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (content != null) "content": content,
      if (fromId != null) "fromId": fromId,
      if (toId != null) "toId": toId,
      if (timeStamp != null) "timeStamp": timeStamp,
      if (sendDate != null) "sendDate": sendDate,
      if (isSeen != null) "isSeen": isSeen,
      if (type != null) "type": type,
      if (mediaType != null) "mediaType": mediaType,
      if (mediaUrl != null) "mediaUrl": mediaUrl,
      if (uploadFinished != null) "uploadFinished": uploadFinished,
      if (reply != null) "reply": reply,
    };
  }

  static Map<String, dynamic> toMap(Message message) {
    return _$MessageToJson(message);
  }
}
