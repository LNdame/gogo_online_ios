import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:gogo_online_ios/src/models/chat_user.dart';
import 'package:gogo_online_ios/src/repository/services/db.dart';


import 'message.dart';

class ChatData {

  late final DB db = DB();

  late final String groupId;
  late final String userId;
  late final String peerId;
  late final ChatUser peer;
  late final List<dynamic> messages;
  late  QueryDocumentSnapshot<Map<String, dynamic>>? lastDoc;
  late int unreadCount;

  ChatData({
    required this.groupId,
    required this.userId,
    required this.peerId,
    required this.peer,
    required this.messages,
    this.lastDoc,
    this.unreadCount = 0,
  });  

  void setLastDoc( QueryDocumentSnapshot<Map<String, dynamic>> doc) {
    lastDoc = doc;
  }

  void addMessage(Message newMsg) {
    if (messages.length > 20) {      
      messages.removeLast();
    }

    messages.insert(0, newMsg);    
  }

  Future<bool> fetchNewChats() async {
    final newData = await db.getNewChats(groupId, lastDoc!);
    await Future.delayed(Duration.zero).then((value) {
      for (var element in newData.docs) {
      // print('new message added -------------> ${element['content']}');
      messages.add(Message.fromMap(element.data()));      
    }

    if (newData.docs.isNotEmpty) {
      lastDoc = newData.docs[newData.docs.length - 1];
    }
    }).then((value) => value);    

    return true;
  }  
}
