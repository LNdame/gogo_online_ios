import '../models/category.dart';
import '../models/healer.dart';
import '../models/media.dart';
import '../models/option.dart';
import '../models/option_group.dart';
import '../models/review.dart';
import 'coupon.dart';

class Product {
late String id;
late  String name;
late  double price;
late  double discountPrice;
late  Media image;
late  String description;
late  String ingredients;
late  String capacity;
late  String unit;
late  String packageItemsCount;
late  bool featured;
late  bool deliverable;
late  Healer healer;
late  Category category;
late  List<Option> options;
late  List<OptionGroup> optionGroups;
late  List<Review> productReviews;

  Product.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      name = jsonMap['name'];
      price = jsonMap['price'] != null ? jsonMap['price'].toDouble() : 0.0;
      discountPrice = jsonMap['discount_price'] != null ? jsonMap['discount_price'].toDouble() : 0.0;
      price = discountPrice != 0 ? discountPrice : price;
      discountPrice = discountPrice == 0 ? discountPrice : jsonMap['price'] != null ? jsonMap['price'].toDouble() : 0.0;
      description = jsonMap['description'];
      capacity = jsonMap['capacity'].toString();
      unit = jsonMap['unit'] != null ? jsonMap['unit'].toString() : '';
      packageItemsCount = jsonMap['package_items_count'].toString();
      featured = jsonMap['featured'] ?? false;
      deliverable = jsonMap['deliverable'] ?? false;
      healer = jsonMap['healer'] != null ? Healer.fromJSON(jsonMap['healer']) : Healer.fromJSON({});
      category = jsonMap['category'] != null ? Category.fromJSON(jsonMap['category']) : Category.fromJSON({});
      image = jsonMap['media'] != null && (jsonMap['media'] as List).isNotEmpty ? Media.fromJSON(jsonMap['media'][0]) :  Media();
      options = jsonMap['options'] != null && (jsonMap['options'] as List).isNotEmpty
          ? List.from(jsonMap['options']).map((element) => Option.fromJSON(element)).toSet().toList()
          : [];
      optionGroups = jsonMap['option_groups'] != null && (jsonMap['option_groups'] as List).isNotEmpty
          ? List.from(jsonMap['option_groups']).map((element) => OptionGroup.fromJSON(element)).toSet().toList()
          : [];
      productReviews = jsonMap['product_reviews'] != null && (jsonMap['product_reviews'] as List).isNotEmpty
          ? List.from(jsonMap['product_reviews']).map((element) => Review.fromJSON(element)).toSet().toList()
          : [];
    } catch (e) {
      id = '';
      name = '';
      price = 0.0;
      discountPrice = 0.0;
      description = '';
      capacity = '';
      unit = '';
      packageItemsCount = '';
      featured = false;
      deliverable = false;
      healer = Healer.fromJSON({});
      category = Category.fromJSON({});
      image = new Media();
      options = [];
      optionGroups = [];
      productReviews = [];
      print(e);
    }
  }

  Map toMap() {
    var map =  <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["price"] = price;
    map["discountPrice"] = discountPrice;
    map["description"] = description;
    map["capacity"] = capacity;
    map["package_items_count"] = packageItemsCount;
    map["unit"] = packageItemsCount;
    map["featured"] = featured;
    map["deliverable"] = deliverable;
    map["healer_id"] = healer.id;
    map["category_id"] = "1";
    return map;
  }

  double getRate() {
    double rate = 0;
    for (var e in productReviews) {
      rate += double.parse(e.rate);
    }
    rate = rate > 0 ? (rate / productReviews.length) : 0;
    return rate;
  }

  @override
  bool operator ==(dynamic other) {
    return other.id == this.id;
  }

  @override
  int get hashCode => this.id.hashCode;

  Coupon applyCoupon(Coupon coupon) {
    if (coupon.code != '') {
      coupon.valid ??= false;
      coupon.discountables.forEach((element) {
        if (element.discountableType == "App\\Models\\Product") {
          if (element.discountableId == id) {
            coupon = _couponDiscountPrice(coupon);
          }
        } else if (element.discountableType == "App\\Models\\Healer") {
          if (element.discountableId == healer.id) {
            coupon = _couponDiscountPrice(coupon);
          }
        } else if (element.discountableType == "App\\Models\\Category") {
          if (element.discountableId == category.id) {
            coupon = _couponDiscountPrice(coupon);
          }
        }
      });
    }
    return coupon;
  }

  Coupon _couponDiscountPrice(Coupon coupon) {
    coupon.valid = true;
    discountPrice = price;
    if (coupon.discountType == 'fixed') {
      price -= coupon.discount;
    } else {
      price = price - (price * coupon.discount / 100);
    }
    if (price < 0) price = 0;
    return coupon;
  }
}
