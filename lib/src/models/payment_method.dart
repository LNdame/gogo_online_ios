import 'package:flutter/widgets.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PaymentMethod {
  late String id;
  late String name;
  late String description;
  late  String logo;
  late  String route;
  late  bool isDefault;
  late  bool selected;

  PaymentMethod(this.id, this.name, this.description, this.route, this.logo, {this.isDefault = false, this.selected = false});
}

class PaymentMethodList {
  late List<PaymentMethod> _paymentsList;
  late List<PaymentMethod> _cashList;
  late List<PaymentMethod> _pickupList;

 PaymentMethodList(BuildContext _context) {
   _paymentsList = [
       PaymentMethod("visacard", AppLocalizations.of(_context)!.visa_card, AppLocalizations.of(_context)!.click_to_pay_with_your_visa_card, "/Checkout", "assets/img/visacard.png",
          isDefault: true),
       PaymentMethod("mastercard", AppLocalizations.of(_context)!.mastercard, AppLocalizations.of(_context)!.click_to_pay_with_your_mastercard, "/Checkout", "assets/img/mastercard.png"),

    ];
   _cashList = [
       PaymentMethod("cod", AppLocalizations.of(_context)!.cash_on_delivery, AppLocalizations.of(_context)!.click_to_pay_cash_on_delivery, "/CashOnDelivery", "assets/img/cash.png"),
    ];
    _pickupList = [
       PaymentMethod("pop", "Tap to Confirm and Pay Now", "By clicking this you agree with the details", "/NetCash", "assets/img/credit-card.png"),
       PaymentMethod("delivery", AppLocalizations.of(_context)!.delivery_address, AppLocalizations.of(_context)!.click_to_pay_on_pickup, "/PaymentMethod", "assets/img/pay_pickup.png"),
    ];
  }

 List<PaymentMethod> get paymentsList => _paymentsList;
  List<PaymentMethod> get cashList => _cashList;
  List<PaymentMethod> get pickupList => _pickupList;
}
