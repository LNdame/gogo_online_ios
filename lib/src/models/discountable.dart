import '../helpers/custom_trace.dart';

class Discountable {
  late String id;
  late String discountableType;
  late String discountableId;

  Discountable.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      discountableType = jsonMap['discountable_type'] != null ? jsonMap['discountable_type'].toString() : "";
      discountableId = jsonMap['discountable_id'] != null ? jsonMap['discountable_id'].toString() : "";
    } catch (e) {
      id = '';
      discountableType = "";
      discountableId = "";
      print(CustomTrace(StackTrace.current, message: e.toString()));
    }
  }

  Map toMap() {
    var map =<String, dynamic>{};
    map["id"] = id;
    map["discountable_type"] = discountableType;
    map["discountable_id"] = discountableId;
    return map;
  }

  @override
  bool operator ==(dynamic other) {
    return other.id == id;
  }

  @override
  int get hashCode => id.hashCode;
}
