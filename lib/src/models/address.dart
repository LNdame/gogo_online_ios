import 'package:location/location.dart';

import '../helpers/custom_trace.dart';

class Address {
  late String id;
  late String? city; //Used to be description
  late String? address;
  late double? latitude;
  late double? longitude;
  late bool isDefault;
  late String userId;

  Address.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      if (jsonMap['description'] != null) {
        city = jsonMap['description'].toString();
      } else {
        city = null;
      }
      address = jsonMap['address'];
      latitude = jsonMap['latitude'] ?? 0.0;
      longitude = jsonMap['longitude'] ?? 0.0;
      isDefault = jsonMap['is_default'] ?? false;
      userId = jsonMap['user_id'] ?? "";
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e.toString()));
    }
  }

  bool isUnknown() => (latitude == null || latitude == 0.0) || (longitude == null || longitude == 0.0);

  Map toMap() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["description"] = city;
    map["address"] = address;
    map["latitude"] = latitude;
    map["longitude"] = longitude;
    map["is_default"] = isDefault;
    map["user_id"] = userId;
    return map;
  }

  LocationData toLocationData() {
    return LocationData.fromMap({
      "latitude": latitude,
      "longitude": longitude,
    });
  }
}
