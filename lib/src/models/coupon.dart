import 'discountable.dart';

class Coupon {
  late String id;
  late String code;
  late  double discount;
  late String discountType;
  late  List<Discountable> discountables;
  late String discountableId;
  late  bool enabled;
  late bool? valid;

  Coupon.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'] != null ? jsonMap['id'].toString() : '';
      code = jsonMap['code'] != null ? jsonMap['code'].toString() : '';
      discount = jsonMap['discount'] != null ? jsonMap['discount'].toDouble() : 0.0;
      discountType = jsonMap['discount_type'] != null ? jsonMap['discount_type'].toString() : "";
      discountables = jsonMap['discountables'] != null ? List.from(jsonMap['discountables']).map((element) => Discountable.fromJSON(element)).toList() : [];
      valid = jsonMap['valid'];
    } catch (e) {
      id = '';
      code = '';
      discount = 0.0;
      discountType = '';
      discountables = [];
      valid = false;
    }
  }

  Map toMap() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["code"] = code;
    map["discount"] = discount;
    map["discount_type"] = discountType;
    map["valid"] = valid;
    map["discountables"] = discountables.map((element) => element.toMap()).toList();

    return map;
  }

  @override
  bool operator ==(dynamic other) {
    return other.id == id;
  }

  @override
  int get hashCode => id.hashCode;
}
