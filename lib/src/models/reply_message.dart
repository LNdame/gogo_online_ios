

import 'package:gogo_online_ios/src/helpers/app_constants.dart';
import 'package:json_annotation/json_annotation.dart';

part 'reply_message.g.dart';

@JsonSerializable()
class ReplyMessage {
  late String? content;
  late String? replierId;
  late String? repliedToId;
  late MessageType? type;

  ReplyMessage({
    this.content,
    this.replierId,
    this.repliedToId,
    this.type,
  });

  Map<String, dynamic> toJson()   {
    return _$ReplyMessageToJson(this);
  }

  factory ReplyMessage.fromJson(Map<String, dynamic> json) {
    return _$ReplyMessageFromJson(json);
  }
}