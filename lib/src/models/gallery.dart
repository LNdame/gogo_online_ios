import '../models/media.dart';

class Gallery {
  late String id;
  late Media image;
  late String description;

  Gallery();

  Gallery.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      image = jsonMap['media'] != null && (jsonMap['media'] as List).isNotEmpty
          ? Media.fromJSON(jsonMap['media'][0])
          : Media();
      description = jsonMap['description'];
    } catch (e) {
      id = '';
      image = Media();
      description = '';
      print(e);
    }
  }
}
