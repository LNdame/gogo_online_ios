import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class OnboardingScreenWidget extends StatefulWidget {
  const OnboardingScreenWidget({Key? key}) : super(key: key);

  @override
  _OnboardingScreenWidgetState createState() => _OnboardingScreenWidgetState();
}

class _OnboardingScreenWidgetState extends State<OnboardingScreenWidget> {
  PageController _controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Container(
            child: PageView(
              controller: _controller,
              children: [
                _slideTemplateImg(
                    title: AppLocalizations.of(context)!.welcome_text,
                    subtitle: AppLocalizations.of(context)!.welcome_subtitle,
                    asset: ""),
                _slideTemplateImg(
                    title: AppLocalizations.of(context)!.book_appointment,
                    subtitle: AppLocalizations.of(context)!.book_appointment_subtitle,
                    asset: ""),
                _slideTemplateImg(
                  title: AppLocalizations.of(context)!.chat_with_specialists,
                  subtitle: AppLocalizations.of(context)!.chat_specialist_subtitle,
                  asset: "",
                ),
              ],
            ),
          )
        ),
        SmoothPageIndicator(
          count: 3,
          controller: _controller,
          effect: const WormEffect(
            dotWidth: 10.0,
            dotHeight: 10.0,
            dotColor: Colors.grey,
          ),
        ),
        const SizedBox(
          height: 15.0,
        ),
      ],
    );
  }

  Widget _slideTemplate({required String title, required String subtitle,required String lottieAsset}) {
    final Size _size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      constraints: BoxConstraints(maxWidth: _size.width * 0.8),
      child: Column(
        children: [
          Expanded(
            child: Lottie.asset(lottieAsset),
          ),
          Text(
            title,
            style: Theme.of(context).textTheme.titleLarge!.merge(TextStyle(color: Theme.of(context).primaryColor)),
          ),
          const SizedBox(height: 5.0),
          Text(
            subtitle,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyMedium!.merge(TextStyle(color: Theme.of(context).primaryColor)),
          ),
          const SizedBox(
            height: 15.0,
          ),
        ],
      ),
    );
  }

  Widget _slideTemplateImg({required String title, required String subtitle, required String asset}) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 00.0),
      constraints: BoxConstraints(maxWidth: size.width ),

      child: Column(
        children: [
          const Expanded(
            child: SizedBox(),
          ),
          Text(
            title,
            style: Theme.of(context).textTheme.titleLarge!.merge(TextStyle(color: Theme.of(context).primaryColor)),
          ),
         const  SizedBox(height: 5.0),
          Text(
            subtitle,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyMedium!.merge(TextStyle(color: Theme.of(context).primaryColor)),
          ),
          const SizedBox(
            height: 15.0,
          ),
        ],
      ),
    );
  }
}
