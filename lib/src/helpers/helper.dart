import 'dart:async';
import 'dart:math';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:html/parser.dart';
import 'dart:ui' as ui;
import 'app_config.dart' as config;
import '../elements/CircularLoadingWidget.dart';
import '../models/consultation.dart';
import '../models/product_consultation.dart';
import '../repository/settings_repository.dart';
class Helper {

  late final BuildContext context;
  late DateTime currentBackPressTime;

  Helper.of(BuildContext ctx) {
    context = ctx;
  }

  // for mapping data retrieved form json array
  static getData(Map<String, dynamic> data) {
    return data['data'] ?? [];
  }

  static bool getBoolData(Map<String, dynamic> data) {
    return (data['data'] as bool) ;
  }

  static int getIntData(Map<String, dynamic> data) {
    return (data['data'] as int) ;
  }

  static String skipHtml(String htmlString) {
    try {
      var document = parse(htmlString);
      String parsedString = parse(document.body?.text).documentElement?.text??"";
      return parsedString;
    } catch (e) {
      return '';
    }
  }

  static Widget getPrice(double myPrice, BuildContext context, {TextStyle? style, String zeroPlaceholder = '-'}) {
    if (style != null) {
      style = style.merge(TextStyle(fontSize: style.fontSize??0.0 + 2));
    }
    try {
      if (myPrice == 0) {
        return Text('-', style: style ?? Theme.of(context).textTheme.subtitle1);
      }
      return RichText(
        softWrap: false,
        overflow: TextOverflow.fade,
        maxLines: 1,
        text: setting.value.currencyRight == false
            ? TextSpan(
          text: setting.value.defaultCurrency,
          style: style == null
              ? Theme.of(context).textTheme.subtitle1?.merge(
            TextStyle(fontWeight: FontWeight.w400, fontSize: Theme.of(context).textTheme.subtitle1?.fontSize??0 - 6),
          )
              : style.merge(TextStyle(fontWeight: FontWeight.w400, fontSize: style.fontSize??0 - 6)),
          children: <TextSpan>[
            TextSpan(text: myPrice.toStringAsFixed(setting.value.currencyDecimalDigits) , style: style ?? Theme.of(context).textTheme.subtitle1),
          ],
        )
            : TextSpan(
          text: myPrice.toStringAsFixed(setting.value.currencyDecimalDigits) ,
          style: style ?? Theme.of(context).textTheme.subtitle1,
          children: <TextSpan>[
            TextSpan(
              text: setting.value.defaultCurrency,
              style: style == null
                  ? Theme.of(context).textTheme.subtitle1?.merge(
                TextStyle(fontWeight: FontWeight.w400, fontSize: Theme.of(context).textTheme.subtitle1?.fontSize??0 - 6),
              )
                  : style.merge(TextStyle(fontWeight: FontWeight.w400, fontSize: style.fontSize??0 - 6)),
            ),
          ],
        ),
      );
    } catch (e) {
      return const Text('');
    }
  }


  static getObjectData(Map<String, dynamic> data) {
    return data['data'] ??  <String, dynamic>{};
  }

  static Future<Uint8List?> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))?.buffer.asUint8List();
  }

  static OverlayEntry overlayLoader(context) {
    OverlayEntry loader = OverlayEntry(builder: (context) {
      final size = MediaQuery.of(context).size;
      return Positioned(
        height: size.height,
        width: size.width,
        top: 0,
        left: 0,
        child: Material(
          color: Theme.of(context).primaryColor.withOpacity(0.85),
          child: CircularLoadingWidget(height: 200),
        ),
      );
    });
    return loader;
  }

  static hideLoader(OverlayEntry loader) {
    Timer(const Duration(milliseconds: 500), () {
      try {
        loader.remove();
        // ignore: empty_catches
      } catch (e) {}
    });
  }

  static Uri getUri(String path) {
    String _path = Uri.parse(GlobalConfiguration().getValue('base_url')).path;
    if (!_path.endsWith('/')) {
      _path += '/';
    }
    Uri uri = Uri(
        scheme: Uri.parse(GlobalConfiguration().getValue('base_url')).scheme,
        host: Uri.parse(GlobalConfiguration().getValue('base_url')).host,
        port: Uri.parse(GlobalConfiguration().getValue('base_url')).port,
        path: _path + path);
    return uri;
  }

  static double getTotalOrderPrice(ProductConsultation productConsultation) {
    double total = productConsultation.price;
    for (var option in productConsultation.options) {
      total += option.price ;
    }
    total *= productConsultation.quantity;
    return total;
  }

  static double getOrderPrice(ProductConsultation productConsultation) {
    double total = productConsultation.price;
    for (var option in productConsultation.options) {
      total += option.price != null ? option.price : 0;
    }
    return total;
  }

  static double getTaxOrder(Consultation consultation) {
    double total = 0;
    for (var productOrder in consultation.productConsultations) {
      total += getTotalOrderPrice(productOrder);
    }
    return consultation.tax * total / 100;
  }

  static double getTotalOrdersPrice(Consultation consultation) {
    double total = 0;
    for (var productConsultation in consultation.productConsultations) {
      total += getTotalOrderPrice(productConsultation);
    }
    total += 0;//order.deliveryFee;
    total += consultation.tax * total / 100;
    return total;
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: "Tap Again to leave the app");
      return Future.value(false);
    }
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    return Future.value(true);
  }

  String trans(String text) {
    switch (text) {
      case "App\\Notifications\\StatusChangedOrder":
        return "Order status changed";
      case "App\\Notifications\\NewOrder":
        return "New order from client";
      case "km":
        return "Km";
      case "mi":
        return "miles";
      default:
        return "";
    }
  }

  static String getCreditCardNumber(String number) {
    String result = '';
    if (number.isNotEmpty && number.length == 16) {
      result = number.substring(0, 4);
      result += ' ' + number.substring(4, 8);
      result += ' ' + number.substring(8, 12);
      result += ' ' + number.substring(12, 16);
    }
    return result;
  }

  static Html applyHtml(context, String html, {TextStyle? style}) {
    return Html(
      data: html ,
      style: {
        "*": Style(
          padding:  EdgeInsets.all(0),
          margin:  Margins.zero,
          color: Theme.of(context).hintColor,
          fontSize: FontSize(16.0),
          display: Display.inlineBlock,           //INLINE_BLOCK,
          width:  Width(config.App(context).appWidth(100)),// config.App(context).appWidth(100),
        ),
        "h4,h5,h6": Style(
          fontSize:  FontSize(18.0),
        ),
        "h1,h2,h3": Style(
          fontSize: FontSize.xLarge,
        ),
        "br": Style(
          height: Height(0),
        ),
        "p": Style(
          fontSize: FontSize(16.0),
        )
      },
    );
  }

  static List<Icon> getStarsList(double rate, {double size = 18}) {
    var list = <Icon>[];
    list = List.generate(rate.floor(), (index) {
      return Icon(Icons.star, size: size, color:const Color(0xFFFFB24D));
    });
    if (rate - rate.floor() > 0) {
      list.add(Icon(Icons.star_half, size: size, color:const Color(0xFFFFB24D)));
    }
    list.addAll(List.generate(5 - rate.floor() - (rate - rate.floor()).ceil(), (index) {
      return Icon(Icons.star_border, size: size, color:const Color(0xFFFFB24D));
    }));
    return list;
  }

  static String limitString(String text, {int limit = 24, String hiddenText = "..."}) {
    return text.substring(0, min<int>(limit, text.length)) + (text.length > limit ? hiddenText : '');
  }
}