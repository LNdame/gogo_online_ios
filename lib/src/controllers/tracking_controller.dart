import 'package:flutter/material.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../helpers/helper.dart';
import '../models/consultation.dart';
import '../models/consultation_status.dart';
import '../repository/consultation_repository.dart';

class TrackingController extends ControllerMVC {
 late Consultation consultation;
  List<ConsultationStatus> consultationStatus = <ConsultationStatus>[];
  GlobalKey<ScaffoldState>? scaffoldKey;
 String trackedOrderId ="";

  TrackingController() {
   scaffoldKey =  GlobalKey<ScaffoldState>();
   consultation = Consultation.fromJSON({});
  }

  void listenForOrder({required String orderId, String? message}) async {
    var stateCtx = state?.context;
    trackedOrderId = orderId;
    final Stream<Consultation> stream = await getConsultation(orderId);
    stream.listen((Consultation cons) {
      setState(() {
        consultation = cons;
      });
    }, onError: (a) {
      print(a);
     // ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
      //  content: Text(S.of(state.context).verify_your_internet_connection),
    //  ));
    }, onDone: () {
      listenForOrderStatus();
      if (message != null) {
        ScaffoldMessenger.of(stateCtx!).showSnackBar(
            SnackBar(content: Text(message),)
        );
      }
    });
  }

  void listenForOrderStatus() async {
    final Stream<ConsultationStatus> stream = await getConsultationStatus();
    stream.listen((ConsultationStatus _orderStatus) {
      setState(() {
        consultationStatus.add(_orderStatus);
      });
    }, onError: (a) {}, onDone: () {});
  }

  List<Step> getTrackingSteps(BuildContext context) {
    List<Step> _orderStatusSteps = [];

    var consultationStatusId = int.tryParse(consultation.consultationStatus.id)??-1;

    this.consultationStatus.forEach((ConsultationStatus _orderStatus) {
      _orderStatusSteps.add(Step(
        state: StepState.complete,
        title: Text(
          _orderStatus.status,
          style: Theme.of(context).textTheme.subtitle1,
        ),
        subtitle: consultation.consultationStatus.id == _orderStatus.id
            ? Text(
                '${DateFormat('HH:mm | yyyy-MM-dd').format(consultation.dateTime)}',
                style: Theme.of(context).textTheme.caption,
                overflow: TextOverflow.ellipsis,
              )
            : SizedBox(height: 0),
        content: SizedBox(
            width: double.infinity,
            child: Text(
              '${Helper.skipHtml(consultation.hint)}',
            )),
        isActive: consultationStatusId >= (int.parse(_orderStatus.id)),
      ));
    });
    return _orderStatusSteps;
  }

  Future<void> refreshOrder() async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey?.currentContext;
    consultation =  Consultation();
    listenForOrder( orderId: trackedOrderId, message: AppLocalizations.of(stateCtx!)!.tracking_refreshed_successfuly);
  }

  void doCancelOrder() {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey?.currentContext;
    cancelConsultation(this.consultation).then((value) {
      setState(() {
        this.consultation.active = false;
      });
    }).catchError((e) {
     /* scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(e),
      ));*/
    }).whenComplete(() {
      consultationStatus = [];
      listenForOrderStatus();
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(stateCtx!)!.consultationThisconsultaitonidHasBeenCanceled as String), //(this.order.id)),
      ));
    });
  }

  bool canCancelOrder(Consultation order) {
    return order.active == true && order.consultationStatus.id == 1;
  }
}
