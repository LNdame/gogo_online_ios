import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../models/cart.dart';
import '../models/coupon.dart';
import '../models/credit_card.dart';
import '../models/consultation.dart';
import '../models/consultation_status.dart';
import '../models/payment.dart';
import '../models/product_consultation.dart';
import '../repository/consultation_repository.dart' as consultationRepo;
import '../repository/settings_repository.dart' as settingRepo;
import '../repository/user_repository.dart' as userRepo;
import 'cart_controller.dart';

class CheckoutController extends CartController {
  Payment? payment;
  CreditCard creditCard = CreditCard();
  bool loading = true;

  CheckoutController() {
    scaffoldKey = GlobalKey<ScaffoldState>();
    listenForCreditCard();
  }

  void listenForCreditCard() async {
    creditCard = await userRepo.getCreditCard();
    setState(() {});
  }

  @override
  void onLoadingCartDone() {
    if (payment != null) addConsultation(carts);
    super.onLoadingCartDone();
  }

  void addConsultation(List<Cart> carts) async {
    Consultation _consultation =  Consultation();
    _consultation.productConsultations = [];
    _consultation.tax = carts[0].product.healer.defaultTax;
   // _consultation.deliveryFee = payment.method == 'Pay on Pickup' ? 0 : 0;// carts[0].product.healer.deliveryFee;
    ConsultationStatus _orderStatus = ConsultationStatus();
    _orderStatus.id = '1'; // TODO default order status Id
    _consultation.consultationStatus = _orderStatus;
    _consultation.billingAddress = settingRepo.billingAddress.value;
    _consultation.consultationDate = carts[0].consultationDate!;
    _consultation.consultationStartTime = carts[0].consultationStartTime;
    _consultation.consultationEndTime = carts[0].consultationEndTime;
    _consultation.hint = ' ';
    carts.forEach((_cart) {
      ProductConsultation productOrder = ProductConsultation();
      productOrder.quantity = _cart.quantity;
      productOrder.price = _cart.product.price;
      productOrder.product = _cart.product;
      productOrder.options = _cart.options;
      _consultation.productConsultations.add(productOrder);
    });
    consultationRepo.addUserConsultation(_consultation, payment!).then((value) async {
      settingRepo.coupon = Coupon.fromJSON({});
      return value;
    }).then((value) {
      if (value is Consultation) {
        setState(() {
          loading = false;
        });
      }
    });
  }

  void updateCreditCard(CreditCard creditCard) {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    userRepo.setCreditCard(creditCard).then((value) {
      setState(() {});
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(stateCtx!)!.payment_card_updated_successfully),
      ));
    });
  }
}
