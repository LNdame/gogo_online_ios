import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../models/consultation.dart';
import '../repository/consultation_repository.dart';

class ConsultationController extends ControllerMVC {
  List<Consultation> consultations = <Consultation>[];
  late GlobalKey<ScaffoldState> scaffoldKey;

  ConsultationController() {
    scaffoldKey =  GlobalKey<ScaffoldState>();
  }

  void listenForConsultations({String? message}) async {
    var scaffoldCtx = scaffoldKey.currentContext;
    final Stream<Consultation> stream = await getConsultations();
    stream.listen((Consultation _consultation) {
      setState(() {
        consultations.add(_consultation);
      });
    }, onError: (a) {
      print(a);
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void listenForHealerConsultations({String? message}) async {
    var scaffoldCtx = scaffoldKey.currentContext;
    final Stream<Consultation> stream = await getHealerConsultations();
    stream.listen((Consultation _consultation) {
      setState(() {
        consultations.add(_consultation);
      });
    }, onError: (a) {
      print(a);
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void doCancelConsultation(Consultation consultation) {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    cancelConsultation(consultation).then((value) {
      setState(() {
        consultation.active = false;
      });
    }).catchError((e) {
      ScaffoldMessenger.of(scaffoldCtx!)
          .showSnackBar(SnackBar(content: Text(e)));
    }).whenComplete(() {
      //refreshOrders();
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(stateCtx!)!
                .consultationThisconsultaitonidHasBeenCanceled
            as String), //(consultation.id)),
      ));
    });
  }

  Future<void> refreshConsultations() async {
    var stateCtx = state?.context;
    consultations.clear();
    listenForConsultations(message: AppLocalizations.of(stateCtx!)!.consultations_refreshed_successfuly);
  }
}
