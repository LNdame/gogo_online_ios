import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gogo_online_ios/route_constants.dart';
import 'package:gogo_online_ios/src/helpers/app_constants.dart';
import 'package:gogo_online_ios/src/helpers/custom_trace.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../repository/user_repository.dart' as user_repo;
import '../repository/settings_repository.dart' as setting_repo;

class OnBoardingController extends ControllerMVC {
  late GlobalKey<ScaffoldState> scaffoldKey;
  static OnBoardingController? _instance;

  factory OnBoardingController() => _instance ??= OnBoardingController._();

  OnBoardingController._() : super() {
    scaffoldKey = GlobalKey<ScaffoldState>();
  }

  @override
  void initState() {
    super.initState();
    try {
      user_repo.getCurrentUser().then((user) => {
            if (user.auth != null)
              {
                if (user.firebaseUid != null && user.firebaseUid!.isNotEmpty)
                  {
                    user.role?.name == AppConstants.ROLE_CLIENT
                        ? setting_repo.navigatorKey.currentState?.pushReplacementNamed('/Pages', arguments: homeIndex)
                        : setting_repo.navigatorKey.currentState
                            ?.pushReplacementNamed('/HealerPages', arguments: homeIndex)
                  }
              }
          });
    } on NoSuchMethodError catch (ne) {
      print(CustomTrace(StackTrace.current, message: ne.toString()));
    } on Exception catch (_) {
      print("error caught");
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e.toString()));
    }
  }
}
