import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../models/consultation.dart';
import '../repository/consultation_repository.dart';

class ProfileController extends ControllerMVC {
  late GlobalKey<ScaffoldState> scaffoldKey;
  static ProfileController? _instance;
  factory ProfileController() => _instance ??= ProfileController._();

  List<Consultation> recentConsultations = [];

  ProfileController._():super() {
    scaffoldKey =  GlobalKey<ScaffoldState>();
    listenForRecentOrders();
  }

  void listenForRecentOrders({String? message}) async {
    final Stream<Consultation> stream = await getRecentConsultations();
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;

    stream.listen((Consultation order) {
      setState(() {
        recentConsultations.add(order);
      });
    }, onError: (a) {
      print(a);
      /* ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(stateCtx!)!.verify_your_internet_connection),
      ));*/
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(content: Text(message)));
        // scaffoldKey?.currentState?.showSnackBar(SnackBar(content: Text(message),));
      }
    });
  }

  Future<void> refreshProfile() async {
    recentConsultations.clear();
    var stateCtx = state?.context;
    listenForRecentOrders(message: AppLocalizations.of(stateCtx!)!.consultations_refreshed_successfuly);
  }
}
