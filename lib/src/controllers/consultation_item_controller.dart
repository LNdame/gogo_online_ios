import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:gogo_online_ios/src/helpers/app_constants.dart';
import 'package:gogo_online_ios/src/models/chat_data.dart';
import 'package:gogo_online_ios/src/models/chat_user.dart';
import 'package:gogo_online_ios/src/models/healer.dart';
import 'package:gogo_online_ios/src/models/message.dart';
import 'package:gogo_online_ios/src/models/user.dart';
import 'package:gogo_online_ios/src/repository/healer_repository.dart';
import 'package:gogo_online_ios/src/repository/services/db.dart';
import 'package:gogo_online_ios/src/repository/settings_repository.dart';
import 'package:mvc_pattern/mvc_pattern.dart';




class ConsultationItemController extends ControllerMVC{
  late Healer healer;
  late ChatUser healerPeer;
   ChatData? chatData;
  late DB db;
 late ChatUser clientPeer;
 late User clientUser;

  ConsultationItemController() {
   /* registerFormKey = new  GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();*/
    healer = Healer.fromJSON({
      'name': 'Gogo'
    });
    clientUser = User.fromJSON({
      'name': 'John Doe'
    });
    healerPeer = ChatUser.fromJson({
    'id':"",
     "username":"",
      'email':"",
      'imageUrl':"",
      'role':"",
      'about':"",


    });
    clientPeer = ChatUser.fromJson({
      'id':"",
      "username":"",
      'email':"",
      'imageUrl':"",
      'role':"",
      'about':"",

    });
    db = DB();
  }

  void listenForHealer({required String id, String? message ,required String currentUserUid}) async {
    final Stream<Healer> stream = await getHealer(id, billingAddress.value);
    stream.listen((Healer _healer) {
      setState(() {
        healer = _healer;
        healerPeer = setupHealerAsPeerUser(_healer);
      } );
    }, onError: (a) {
      print(a);

    }, onDone: () {
      if(currentUserUid!=null){
         listenForChatData(currentUserUid, healerPeer);
      }
      if (message != null) {

      }
    });
  }

//Id here should be PatientID //
  void listenForPatient({required User clUser, String? message ,required String currentUserUid}) async {
    setState(() {
      clientUser = clUser;
      clientPeer = setupClientAsPeerUser(clUser);
    });
    if(currentUserUid!=null){
      listenForChatDataForHealer(currentUserUid, clientPeer);
    }
  }

  ChatUser setupHealerAsPeerUser(Healer healer) {
    return ChatUser(
        id: healer.firebaseId,
        username: healer.name,
        email: "",
        imageUrl: healer.image?.thumb,
        about: healer.description,
        role: AppConstants.ROLE_MANAGER);
  }

  ChatUser setupClientAsPeerUser(User client) {
    return  ChatUser(
        id: client.firebaseUid??"",
        username: client.name??"",
        email: "",
        imageUrl: client.image?.thumb,
        about: "",
        role: AppConstants.ROLE_CLIENT);
  }

  void listenForChatData(String currentUserUid, ChatUser peer) async{
    final Stream<ChatData> stream = await getChatData(currentUserUid , peer);
    stream.listen((ChatData data) {
      setState(() => chatData = data);
    }, onError: (e){}, onDone: (){});
  }

  void listenForChatDataForHealer(String currentUserUid, ChatUser peer) async{
    final Stream<ChatData> stream = await getChatDataForHealer(currentUserUid , peer);
    stream.listen((ChatData data) {
      setState(() => chatData = data);
    }, onError: (e){}, onDone: (){});
  }

  Future<void> retrieveChatData(String currentUserUid, ChatUser peer) async{
    final Stream<ChatData> stream = await getChatData(currentUserUid , peer);
    stream.listen((ChatData data) {
      setState(() => chatData = data);
    }, onError: (e){}, onDone: (){});
  }

  String getGroupId(String uid) {
    String groupId;
    if (uid.hashCode <= healer.firebaseId.hashCode) {
      groupId = '$uid-${healer.firebaseId}';
    } else {
      groupId = '${healer.firebaseId}-$uid';
    }

    return groupId;
  }

  String getGroupIdForHealer(String uid) {
    String groupId;
    if (uid.hashCode <= clientUser.firebaseUid.hashCode) {
      groupId = '$uid-${clientUser.firebaseUid}';
    } else {
      groupId = '${clientUser.firebaseUid}-$uid';
    }

    return groupId;
  }

  Future<Stream<ChatData>> getChatData(String currentUserUid, ChatUser peer) async {
    String groupId = getGroupId(currentUserUid);
    final messagesData = await db.getChatItemData(groupId);

    int unreadCount = 0;
    List<Message> messages = [];
    for (int i = 0; i < messagesData.docs.length; i++) {
      var tmp = Message.fromMap(  messagesData.docs[i].data()); //Map<String, dynamic>.from(
      messages.add(tmp);
      if(tmp.fromId == healer.firebaseId && !tmp.isSeen!) unreadCount++;
    }

   late QueryDocumentSnapshot<Map<String, dynamic>> lastDoc;
    if (messagesData.docs.isNotEmpty) {
      lastDoc = messagesData.docs[messagesData.docs.length - 1];
    }

    ChatData chatData = ChatData(
      userId: currentUserUid,
      peerId: healer.firebaseId,
      groupId: groupId,
      peer: peer,
      messages: messages,
      lastDoc: lastDoc,
      unreadCount: unreadCount,
    );
    return new Stream.value(chatData);
  }

  Future<Stream<ChatData>> getChatDataForHealer(String currentUserUid, ChatUser peer) async {
    String groupId = getGroupIdForHealer(currentUserUid);
    final messagesData = await db.getChatItemData(groupId);

    int unreadCount = 0;
    List<Message> messages = [];
    for (int i = 0; i < messagesData.docs.length; i++) {
      var tmp = Message.fromMap(messagesData.docs[i].data());//Map<String, dynamic>.from(
      messages.add(tmp);
      if(tmp.fromId == clientUser.firebaseUid && !tmp.isSeen!) unreadCount++;
    }

    late QueryDocumentSnapshot<Map<String, dynamic>> lastDoc;
    if (messagesData.docs.isNotEmpty) {
      lastDoc = messagesData.docs[messagesData.docs.length - 1];
    }

    var clientFireUid =clientUser.firebaseUid??"";

    ChatData chatData = ChatData(
      userId: currentUserUid,
      peerId: clientFireUid,
      groupId: groupId,
      peer: peer,
      messages: messages,
      lastDoc: lastDoc,
      unreadCount: unreadCount,
    );
    return new Stream.value(chatData);
  }
}