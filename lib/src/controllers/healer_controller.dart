
import 'package:flutter/material.dart';
import 'package:gogo_online_ios/src/helpers/app_constants.dart';
import 'package:gogo_online_ios/src/helpers/helper.dart';
import 'package:gogo_online_ios/src/models/chat_data.dart';
import 'package:gogo_online_ios/src/models/chat_user.dart';
import 'package:gogo_online_ios/src/models/message.dart';
import 'package:gogo_online_ios/src/repository/services/db.dart';
import 'package:gogo_online_ios/src/repository/user_repository.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../models/category.dart';
import '../models/gallery.dart';
import '../models/healer.dart';
import '../models/product.dart';
import '../models/review.dart';
import '../repository/category_repository.dart';
import '../repository/gallery_repository.dart';
import '../repository/healer_repository.dart' ;
import '../repository/product_repository.dart';
import '../repository/settings_repository.dart';

class HealerController extends ControllerMVC {
 late Healer healer;
 late ChatUser healerPeer;
 late ChatData? chatData;
  List<Gallery> galleries = <Gallery>[];
  List<Product> products = <Product>[];
  List<Category> categories = <Category>[];
  List<Product> trendingProducts = <Product>[];
  List<Product> featuredProducts = <Product>[];
  List<Review> reviews = <Review>[];
 late  GlobalKey<ScaffoldState> scaffoldKey;
 late GlobalKey<FormState> registerFormKey;
 late OverlayEntry loader;
 late DB db;

  HealerController() {
    registerFormKey =   GlobalKey<FormState>();
    scaffoldKey =  GlobalKey<ScaffoldState>();
    db = DB();
    healer = Healer.fromJSON({});
  }

  void listenForHealer({required String id, String? message ,  String? currentUserUid}) async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    final Stream<Healer> stream = await getHealer(id, billingAddress.value);
    stream.listen((Healer _healer) {
      setState(() {
        healer = _healer;
        healerPeer = setupHealerAsPeerUser(_healer);
      } );
    }, onError: (a) {
      print(a);
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(stateCtx!)!.verify_your_internet_connection),
      ));
    }, onDone: () {
      if(currentUserUid!=null){
        listenForChatData(currentUserUid, healerPeer);
      }
      if (message != null) {
        ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void listenForChatData(String currentUserUid, ChatUser peer) async{
    final Stream<ChatData> stream = await getChatData(currentUserUid , peer);
    stream.listen((ChatData _data) {
      setState(() => chatData = _data);
    }, onError: (e){}, onDone: (){});
  }

  void listenForGalleries(String idHealer) async {
    final Stream<Gallery> stream = await getGalleries(idHealer);
    stream.listen((Gallery _gallery) {
      setState(() => galleries.add(_gallery));
    }, onError: (a) {}, onDone: () {});
  }

  void listenForHealerReviews({required String id, String? message}) async {
    final Stream<Review> stream = await getHealerReviews(id);
    stream.listen((Review _review) {
      setState(() => reviews.add(_review));
    }, onError: (a) {}, onDone: () {});
  }

  void listenForProducts(String idHealer, {List<String>? categoriesId}) async {
    final Stream<Product> stream = await getProductsOfHealer(idHealer, categories: categoriesId);
    stream.listen((Product _product) {
      setState(() => products.add(_product));
    }, onError: (a) {
      print(a);
    }, onDone: () {
      try {
        healer.name = products.elementAt(0).healer.name;
      } catch (e) {
        print(e);
      }
    });
  }

  void listenForTrendingProducts(String idHealer) async {
    final Stream<Product> stream = await getTrendingProductsOfHealer(idHealer);
    stream.listen((Product _product) {
      setState(() => trendingProducts.add(_product));
    }, onError: (a) {
      print(a);
    }, onDone: () {});
  }

  void listenForFeaturedProducts(String idHealer) async {
    final Stream<Product> stream = await getFeaturedProductsOfHealer(idHealer);
    stream.listen((Product _product) {
      setState(() => featuredProducts.add(_product));
    }, onError: (a) {
      print(a);
    }, onDone: () {});
  }

  Future<void> listenForCategories(String healerId) async {
    var stateCtx = state?.context;
    final Stream<Category> stream = await getCategoriesOfHealer(healerId);
    stream.listen((Category _category) {
      setState(() => categories.add(_category));
    }, onError: (a) {
      print(a);
    }, onDone: () {
      categories.insert(0, Category.fromJSON({'id': '0', 'name': AppLocalizations.of(stateCtx!)!.all}));
    });
  }

  Future<void> selectCategory(List<String> categoriesId) async {
    products.clear();
    listenForProducts(healer.id, categoriesId: categoriesId);
  }

  Future<void> refreshHealer() async {
    var stateCtx = state?.context;
    var _id = healer.id;
    healer =  Healer.fromJSON({});
    galleries.clear();
    reviews.clear();
    featuredProducts.clear();
    listenForHealer(id: _id, message: AppLocalizations.of(stateCtx!)!.healer_refreshed_successfuly);
    listenForHealerReviews(id: _id);
    listenForGalleries(_id);
    listenForFeaturedProducts(_id);
  }

  void requestRegisterHealer(Healer healer) async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    loader = Helper.overlayLoader(stateCtx!);
    FocusScope.of(stateCtx).unfocus();
    if (registerFormKey.currentState?.validate()!=null && !registerFormKey.currentState!.validate()) {
      registerFormKey.currentState?.save();
      Overlay.of(stateCtx)?.insert(loader);
      registerHealer(healer).then((value) {
        if (value != null) {
          Product newConsultation = setupVirtualConsultation(healer.hourlyPrice, value);
          storeProduct(newConsultation).then((value) {
            if (value != null) {
              ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(const SnackBar(
                content: Text("Request sent and consultation rate set!"),
              ));
            }
          });

          Navigator.of(stateCtx).pushReplacementNamed('/HealerRegisterSuccess');
        } else {
          ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(const SnackBar(
            content: Text("Error!!"),
          ));
        }
      }).catchError((error) {
        loader.remove();
        ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
          content: Text("Error!!"),
        ));
      }).whenComplete(() => Helper.hideLoader(loader));
    }
  }

  Product setupVirtualConsultation(double price, Healer healer){
    var product =  Product.fromJSON({});
    product.name = "Virtual Consultation";
    product.price = price;
    product.description ="Chat or video call with the healer";
    product.capacity = "1";
    product.packageItemsCount="1";
    product.unit = "per hour";
    product.featured = false;
    product.deliverable = false;
    product.healer = healer;

    return product;
  }


  /// TODO retrieve chat data with current user if exist

  ChatUser setupHealerAsPeerUser(Healer healer) {
    return  ChatUser(
        id: healer.firebaseId,
        username: healer.name,
        email: "",
        imageUrl: healer.image?.thumb,
        about: healer.description,
        role: AppConstants.ROLE_MANAGER);
  }

  String getGroupId(String uid) {
    String groupId;
    if (uid.hashCode <= healer.firebaseId.hashCode) {
      groupId = '$uid-${healer.firebaseId}';
    } else {
      groupId = '${healer.firebaseId}-$uid';
    }

    return groupId;
  }

  Future<Stream<ChatData>> getChatData(String currentUserUid, ChatUser peer) async {
    String groupId = getGroupId(currentUserUid);
   // final peer = await db.getUser(healer.firebaseId);
    //final User person = User.fromJson(peer.data);
    final messagesData = await db.getChatItemData(groupId);

    int unreadCount = 0;
    List<Message> messages = [];
    for (int i = 0; i < messagesData.docs.length; i++) {
      var tmp = Message.fromMap(Map<String, dynamic>.from(messagesData.docs[i].data()));
      messages.add(tmp);
      if(tmp.fromId == healer.firebaseId && !tmp.isSeen!) unreadCount++;
    }

    var lastDoc;
    if (messagesData.docs.isNotEmpty) {
      lastDoc = messagesData.docs[messagesData.docs.length - 1];
    }


    ChatData chatData = ChatData(
      userId: currentUserUid,
      peerId: healer.firebaseId,
      groupId: groupId,
      peer: peer,
      messages: messages,
      lastDoc: lastDoc,
      unreadCount: unreadCount,
    );
    return  Stream.value(chatData);
  }

}
