//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:gogo_online_ios/src/repository/services/db.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../../route_constants.dart';
import '../helpers/helper.dart';
import '../models/user.dart' as user_local;
import '../repository/user_repository.dart' as user_repo;

class UserController extends ControllerMVC {
  static UserController? _instance;

  factory UserController() => _instance ??= UserController._();

  late GlobalKey<FormState> loginFormKey;
  late GlobalKey<ScaffoldState> scaffoldKey;
  late FirebaseAuth _firebaseAuth;
  late OverlayEntry loader;
  user_local.User user = user_local.User.fromJSON({});
  bool hidePassword = true;
  bool loading = false;

  UserController._() : super() {
    loginFormKey = GlobalKey<FormState>();
    scaffoldKey = GlobalKey<ScaffoldState>();
    _firebaseAuth = FirebaseAuth.instance;
  }


 /* UserController() {
    loginFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    _firebaseAuth = FirebaseAuth.instance;
    // _firebaseMessaging = FirebaseMessaging();
    *//* _firebaseMessaging.getToken().then((String _deviceToken) {
      user.deviceToken = _deviceToken;
    }).catchError((e) {
      print('Notification not configured');
    });*//*
  }*/

//TODO look at the update if that is not a better way
  void login() async {
    loader = Helper.overlayLoader(state?.context);
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;

    FocusScope.of(stateCtx!).unfocus();
    var isFormValid =loginFormKey.currentState?.validate();
    if (isFormValid! ) {
      loginFormKey.currentState?.save();
      Overlay.of(stateCtx)?.insert(loader);
      user_repo.login(user).then((value) {
        if (value != null && value.apiToken != null) {
          if (value.role?.name == "manager") {
            Navigator.of(scaffoldCtx!).pushReplacementNamed('/HealerPages', arguments: homeIndex);
          } else {
            Navigator.of(scaffoldCtx!).pushReplacementNamed('/Pages', arguments: homeIndex);
          }
          firebaseSilentLogin(user.email, user.password);
        } else {
          ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
            content: Text(AppLocalizations.of(stateCtx)!.wrong_email_or_password),
          ));
        }
      }).catchError((e) {
        loader.remove();
        ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
          content: Text(AppLocalizations.of(stateCtx)!.this_account_not_exist),
        ));
      }).whenComplete(() {
        Helper.hideLoader(loader);
      });
    }
  }

  //to deprecate
  void register() async {
    loader = Helper.overlayLoader(state?.context);
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;

    FocusScope.of(stateCtx!).unfocus();
    if (loginFormKey.currentState?.validate()!=null  && !loginFormKey.currentState!.validate() ) {
      loginFormKey.currentState?.save();
      Overlay.of(stateCtx)?.insert(loader);
      user_repo.register(user).then((value) {
        if (value != null && value.apiToken != null) {
          registerOnFirebase(value, user.password??"");
          Navigator.of(scaffoldCtx!).pushReplacementNamed('/Pages', arguments: homeIndex);
        } else {
          ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
            content: Text(AppLocalizations.of(stateCtx)!.wrong_email_or_password),
          ));
        }
      }).catchError((e) {
        loader.remove();
        ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
          content: Text(AppLocalizations.of(stateCtx)!.this_email_account_exists),
        ));
      }).whenComplete(() {
        Helper.hideLoader(loader);
      });
    }
  }

  void registerFirebaseFirst() async {
    loader = Helper.overlayLoader(state?.context);
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    FocusScope.of(stateCtx!).unfocus();

    if (loginFormKey.currentState?.validate()!=null  && loginFormKey.currentState!.validate()) {
      loginFormKey.currentState?.save();
      Overlay.of(stateCtx)?.insert(loader);
      await registerFirstOnFirebase(user, user.password??"").then((userCred) {
        user.firebaseUid = userCred.user?.uid;
        user_repo.register(user).then((value) {
          if (value != null && value.apiToken != null) {
            try {
              final db = DB();
              db.addNewUser(userCred.user?.uid??"", value.image?.thumb??"", user.name??"", user.email??"");
              userCred.user?.updateDisplayName(user.name);
              user_repo.currentUser.value.firebaseUid = userCred.user?.uid;
              user_repo.setCurrentUserFireBaseUid(userCred.user?.uid??"");

            } catch (error) {
              print(error);
            }
          }
        }).whenComplete(() {
          Helper.hideLoader(loader);
          Navigator.of(scaffoldCtx!).pushReplacementNamed('/Pages', arguments: homeIndex);
        } );
      });
    }
  }

  void resetPassword() {
    loader = Helper.overlayLoader(state?.context);
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    FocusScope.of(stateCtx!).unfocus();

    if (loginFormKey.currentState?.validate()!=null  && loginFormKey.currentState!.validate()) {
      loginFormKey.currentState?.save();
      Overlay.of(stateCtx)?.insert(loader);
      user_repo.resetPassword(user).then((value) {
        if (value != null && value == true) {
          ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
            content: Text(AppLocalizations.of(stateCtx)!.your_reset_link_has_been_sent_to_your_email),
            action: SnackBarAction(
              label: AppLocalizations.of(stateCtx)!.login,
              onPressed: () {
                Navigator.of(scaffoldCtx).pushReplacementNamed('/Login');
              },
            ),
            duration: const Duration(seconds: 10),
          ));
        } else {
          loader.remove();
          ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
            content: Text(AppLocalizations.of(stateCtx)!.error_verify_email_settings),
          ));
        }
      }).whenComplete(() {
        Helper.hideLoader(loader);
      });
    }
  }

  void registerOnFirebase(user_local.User user, String password) {
    _firebaseAuth.createUserWithEmailAndPassword(email: user.email??="", password: password).then((userCred) {
      try {
        final db = DB();
        db.addNewUser(userCred.user?.uid??"", user.image?.thumb??"", user.name??"", user.email??"");
        userCred.user?.updateDisplayName(user.name);

        user_repo.currentUser.value.firebaseUid = userCred.user?.uid;
        user_repo.setCurrentUserFireBaseUid(userCred.user?.uid??"");
      } catch (error) {
        print(error);
      }
    });
  }

  Future<UserCredential> registerFirstOnFirebase(user_local.User user, String password){
    return _firebaseAuth.createUserWithEmailAndPassword(email: user.email??="", password: password);
  }

  void firebaseSilentLogin(email, password) {
    _firebaseAuth.signInWithEmailAndPassword(email: email, password: password).then((value) {
      user_repo.currentUser.value.firebaseUid = value.user?.uid;
      user_repo.setCurrentUserFireBaseUid(value.user?.uid??"");
    });
  }
}
