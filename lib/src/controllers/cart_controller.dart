import 'package:flutter/material.dart';
import 'package:gogo_online_ios/route_constants.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../helpers/helper.dart';
import '../models/cart.dart';
import '../models/coupon.dart';
import '../repository/cart_repository.dart';
import '../repository/coupon_repository.dart';
import '../repository/settings_repository.dart';
import '../repository/user_repository.dart';

class CartController extends ControllerMVC {
  List<Cart> carts = <Cart>[];
  double taxAmount = 0.0;
  double deliveryFee = 0.0;
  int cartCount = 0;
  double subTotal = 0.0;
  double total = 0.0;
  late GlobalKey<ScaffoldState> scaffoldKey;
  ValueNotifier<List<Cart>> sumCarts =  ValueNotifier([]);

 // static CartController? _instance;
 // factory CartController() => _instance ??= CartController._();


  CartController() {
    scaffoldKey =  GlobalKey<ScaffoldState>();
  }

  void listenForCarts({String? message}) async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    carts.clear();
    final Stream<Cart> stream = await getCart();
    stream.listen((Cart _cart) {
      if (!carts.contains(_cart)) {
        setState(() {
          coupon = _cart.product.applyCoupon(coupon);
          carts.add(_cart);
        });
      }
    }, onError: (a) {
      print(a);
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(
            AppLocalizations.of(stateCtx!)!.verify_your_internet_connection),
      ));
    }, onDone: () {
      if (carts.isNotEmpty) {
        calculateSubtotal();
        sumCarts.value = carts;
      }
      if (message != null) {
        ScaffoldMessenger.of(scaffoldCtx!)
            .showSnackBar(SnackBar(content: Text(message)));
      }
      onLoadingCartDone();
    });
  }

  void onLoadingCartDone() {}

  void listenForCartsCount({String? message}) async {
    final Stream<int> stream = await getCartCount();
    stream.listen((int _count) {
      setState(() {
        this.cartCount = _count;
      });
    }, onError: (a) {
      print(a);
      /*ScaffoldMessenger.of(scaffoldKey?.currentContext).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(state.context).verify_your_internet_connection),
      ));*/
    });
  }

  Future<void> refreshCarts() async {
    var stateCtx = state?.context;
    setState(() {
      carts = [];
    });
    listenForCarts(message: AppLocalizations.of(stateCtx!)!.carts_refreshed_successfuly);
  }

  void removeFromCart(Cart _cart) async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    setState(() {
      this.carts.remove(_cart);
    });
    removeCart(_cart).then((value) {
      calculateSubtotal();
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(  //TODO: add argument to intl locale
        content: Text(AppLocalizations.of(stateCtx!)!.the_product_was_removed_from_your_cart as String),//(_cart.product.name)),
      ));
    });
  }

  void calculateSubtotal() async {
    double cartPrice = 0;
    subTotal = 0;
    carts.forEach((cart) {
      cartPrice = cart.product.price;
      cart.options.forEach((element) {
        cartPrice += element.price;
      });
      cartPrice *= cart.quantity;
      subTotal += cartPrice;
    });

    deliveryFee = 0;

    taxAmount = (subTotal + deliveryFee) * carts[0].product.healer.defaultTax / 100;
    total = subTotal + taxAmount + deliveryFee;
    setState(() {});
  }

  void doApplyCoupon(String code, {String? message}) async {

    coupon =  Coupon.fromJSON({"code": code, "valid": null});
    final Stream<Coupon> stream = await verifyCoupon(code);
    stream.listen((Coupon _coupon) async {
      coupon = _coupon;
    }, onError: (a) {
      print(a);

    }, onDone: () {
      listenForCarts();
    });
  }

  incrementQuantity(Cart cart) {
    if (cart.quantity <= 99) {
      ++cart.quantity;
      updateCart(cart);
      calculateSubtotal();
    }
  }

  decrementQuantity(Cart cart) {
    if (cart.quantity > 1) {
      --cart.quantity;
      updateCart(cart);
      calculateSubtotal();
    }
  }

  updateDateTime(Cart cart){
    updateCart(cart);
  }

  void goCheckout(BuildContext context) {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    if (!currentUser.value.profileCompleted()) {
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(stateCtx!)!.completeYourProfileDetailsToContinue),
        action: SnackBarAction(
          label: AppLocalizations.of(stateCtx)!.settings,
          textColor: Theme.of(stateCtx).colorScheme.secondary,
          onPressed: () {
            Navigator.of(stateCtx).pushNamed('/Settings');
          },
        ),
      ));
    } else {
      if (carts[0].product.healer.closed) {
        ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
          content: Text(AppLocalizations.of(stateCtx!)!.this_healer_is_closed_),
        ));
      } else {
        Navigator.of(stateCtx!).pushNamed(consultationSummaryRoute);
      }
    }
  }

  Color getCouponIconColor() {
    var stateCtx = state?.context;
    if (coupon?.valid == true) {
      return Colors.green;
    } else if (coupon?.valid == false) {
      return Colors.redAccent;
    }
    return Theme.of(stateCtx!).focusColor.withOpacity(0.7);
  }
}
