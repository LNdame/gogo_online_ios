
import 'package:gogo_online_ios/src/models/model.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../main.dart';

class GenericController extends ControllerMVC{
  static GenericController? _this;
  factory GenericController([StateMVC? state])=> _this ??= GenericController._(state);
  GenericController._(StateMVC? state):_model = Model(),super(state);

  final Model _model;

  int get count => _model.counter;

  // The Controller knows how to 'talk to' the Model and to the View (interface).
  void incrementCounter() {
    //
    _model.incrementCounter();

    /// Retrieve a particular State object. The rest is ignore if not at 'HomePage'
  //  final homeState = stateOf<MyHomePage>();

    /// If working with a particular State object and if divisible by 5
    //if (homeState != null && _model.counter % 5 == 0) {
      //
    ///  dataObject = _model.sayHello();
   // }

    /// Just rebuild an InheritedWidget's dependencies.
    buildInherited();
  }

  /// Call the State object's setState() function to reflect the change.
  void onPressed() => setState(() => _model.incrementCounter());


}