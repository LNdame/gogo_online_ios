import 'dart:async';

//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../helpers/custom_trace.dart';
import '../repository/settings_repository.dart' as setting_repo;
import '../repository/user_repository.dart' as user_repo;

class SplashScreenController extends ControllerMVC {
  /*static SplashScreenController? _this;
  factory SplashScreenController() => _this ??= SplashScreenController._();
  SplashScreenController._():super();*/

  ValueNotifier<Map<String, double>> progress =  ValueNotifier( <String, double>{});
  late GlobalKey<ScaffoldState> scaffoldKey;
 // final FirebaseMessaging firebaseMessaging = FirebaseMessaging();

 SplashScreenController() {
    scaffoldKey =  GlobalKey<ScaffoldState>();
    // Should define these variables before the app loaded
    progress.value = {"Setting": 0, "User": 0};
  }

  @override
  void initState() {
    super.initState();
    progress.value = {"Setting": 0, "User": 0};
  //  firebaseMessaging.requestNotificationPermissions(const IosNotificationSettings(sound: true, badge: true, alert: true));
    //configureFirebase(firebaseMessaging);
    try{
      user_repo.currentUser.addListener(() {
        if (user_repo.currentUser.value.auth != null) {
          progress.value["User"] = 100;
          // ignore: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member
          progress.notifyListeners();
        }
      });
    }catch (e){
      print(CustomTrace(StackTrace.current, message: e.toString()));
    }

    Timer(Duration(seconds: 2), () {
        setting_repo.navigatorKey.currentState?.pushReplacementNamed('/Onboarding');
    });
  }

  /*void configureFirebase(FirebaseMessaging _firebaseMessaging) {
    try {
      _firebaseMessaging.configure(
        onMessage: notificationOnMessage,
        onLaunch: notificationOnLaunch,
        onResume: notificationOnResume,
      );
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e));
      print(CustomTrace(StackTrace.current, message: 'Error Config Firebase'));
    }
  }*/

  Future notificationOnResume(Map<String, dynamic> message) async {
    try {
      if (message['data']['id'] == "orders") {
        setting_repo.navigatorKey.currentState?.pushReplacementNamed('/Pages', arguments: 3);
      }
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e.toString()));
    }
  }

  Future notificationOnLaunch(Map<String, dynamic> message) async {
    String messageId = await setting_repo.getMessageId();
    try {
      if (messageId != message['google.message_id']) {
        if (message['data']['id'] == "orders") {
          await setting_repo.saveMessageId(message['google.message_id']);
          setting_repo.navigatorKey.currentState?.pushReplacementNamed('/Pages', arguments: 3);
        }
      }
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e.toString()));
    }
  }

  Future notificationOnMessage(Map<String, dynamic> message) async {
    Fluttertoast.showToast(
      msg: message['notification']['title'],
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.TOP,
      timeInSecForIosWeb: 5,
    );
  }
}
