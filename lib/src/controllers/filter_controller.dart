import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gogo_online_ios/src/helpers/app_constants.dart';
import 'package:gogo_online_ios/src/models/province.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../models/cart.dart';
import '../models/field.dart';
import '../models/filter.dart';
import '../repository/field_repository.dart';

class FilterController extends ControllerMVC {
  late GlobalKey<ScaffoldState> scaffoldKey;
  List<Field> fields = [];
  List<Province> provinces = [];

  late Filter filter;
  late  Cart cart;

  static FilterController? _instance;
  factory FilterController()=> _instance ??= FilterController._();

  FilterController._() {
    scaffoldKey =  GlobalKey<ScaffoldState>();

    listenForFilter().whenComplete(() {
      listenForFields();
      populateProvinces();
    });
  }

  Future<void> listenForFilter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      filter = Filter.fromJSON(json.decode(prefs.getString('filter') ?? '{}'));
    });
  }

  Future<void> saveFilter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    filter.fields = this.fields.where((_f) => _f.selected).toList();
    prefs.setString('filter', json.encode(filter.toMap()));
  }

  void listenForFields({String? message}) async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    fields.add(Field.fromJSON({'id': '0', 'name': AppLocalizations.of(stateCtx!)!.all, 'selected': true}));
    final Stream<Field> stream = await getFields();
    stream.listen((Field _field) {
      setState(() {
        if (filter.fields.contains(_field)) {
          _field.selected = true;
          fields.elementAt(0).selected = false;
        }
        fields.add(_field);
      });
    }, onError: (a) {
      print(a);
     /* ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(S.of(stateCtx!).verify_your_internet_connection),
      ));*/
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  void populateProvinces() {
    provinces = [
      Province(name: "All", selected: false),
      Province(name: AppConstants.EASTERN_CAPE, selected: false),
      Province(name: AppConstants.FREE_STATE, selected: false),
      Province(name: AppConstants.GAUTENG, selected: false),
      Province(name: AppConstants.KWAZULU_NATAL, selected: false),
      Province(name: AppConstants.LIMPOPO, selected: false),
      Province(name: AppConstants.MPUMALANGA, selected: false),
      Province(name: AppConstants.NORTHERN_CAPE, selected: false),
      Province(name: AppConstants.NORTH_WEST, selected: false),
      Province(name: AppConstants.WESTERN_CAPE, selected: false)
    ];
    filter.provinces = provinces;
  }

  Future<void> refreshFields() async {
    var stateCtx = state?.context;
    fields.clear();
    listenForFields(message: AppLocalizations.of(stateCtx!)!.addresses_refreshed_successfuly);
  }

  void clearFilter() {
    setState(() {
      filter.isCurrentCity = false;
      filter.open = false;
      filter.delivery = false;
      resetFields();
      resetProvinces();
    });
  }

  void resetFields() {
    filter.fields = [];
    fields.forEach((Field _f) {
      _f.selected = false;
    });
    fields.elementAt(0).selected = true;
  }

  void resetProvinces() {
    filter.provinces = [];
    provinces.forEach((Province _p) {
      _p.selected = false;
    });
    provinces.elementAt(0).selected = true;
  }

  void onChangeFieldsFilter(int index) {
    if (index == 0) {
      // all
      setState(() {
        resetFields();
      });
    } else {
      setState(() {
        fields.elementAt(index).selected = !fields.elementAt(index).selected;
        fields.elementAt(0).selected = false;
      });
    }
  }

  void onChangeProvincesFilter(int index) {
    if (index == 0) {
      // all
      setState(() {
        resetProvinces();
      });
    } else {
      setState(() {
        provinces.elementAt(index).selected = !provinces.elementAt(index).selected!;
        provinces.elementAt(0).selected = false;
      });
    }
  }
}
