import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../models/address.dart' as model;
import '../models/cart.dart';
import '../repository/cart_repository.dart';
import '../repository/settings_repository.dart' as setting_repo;
import '../repository/user_repository.dart' as user_repo;

class BillingAddressesController extends ControllerMVC with ChangeNotifier {
  List<model.Address> addresses = <model.Address>[];
  late GlobalKey<ScaffoldState> scaffoldKey;
  late Cart cart;

  BillingAddressesController() {
   scaffoldKey = GlobalKey<ScaffoldState>();
    listenForAddresses();
  }

  void listenForAddresses({String? message}) async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    setting_repo.getCurrentLocation().then((value) {
      if(!value.isUnknown()){
        setState(() {
          addresses.add(value);
        });
      }
    });
    /*final Stream<model.Address> stream = await user_repo.getAddresses();
    stream.listen((model.Address _address) {
      setState(() {
        addresses.add(_address);
      });
    }, onError: (a) {
      print(a);
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(
            AppLocalizations.of(stateCtx!)!.verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        ScaffoldMessenger.of(scaffoldCtx!)
            .showSnackBar(SnackBar(content: Text(message)));
      }
    });*/
  }

  void listenForCart() async {
    final Stream<Cart> stream = await getCart();
    stream.listen((Cart _cart) {
      cart = _cart;
    });
  }

  Future<void> refreshAddresses() async {
    addresses.clear();
    var stateCtx = state?.context;
    listenForAddresses(message: AppLocalizations.of(stateCtx!)!.addresses_refreshed_successfuly);
  }

  Future<void> changeBillingAddress(model.Address address) async {
    await setting_repo.changeCurrentLocation(address);
    setState(() {
      setting_repo.billingAddress.value = address;
    });
    setting_repo.billingAddress.notifyListeners();
  }

  Future<void> changeBillingAddressToCurrentLocation() async {
    model.Address address = await setting_repo.setCurrentLocation();
    setState(() {
      setting_repo.billingAddress.value = address;
    });
    setting_repo.billingAddress.notifyListeners();
  }

  void addAddress(model.Address address) {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    user_repo.addAddress(address).then((value) {
      setState(() {
        addresses.insert(0, value);
      });
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(stateCtx!)!.new_address_added_successfully),
      ));
    });
  }

  void chooseBillingAddress(model.Address address) {
    setState(() {
      setting_repo.billingAddress.value = address;
    });
    setting_repo.billingAddress.notifyListeners();
  }

  void updateAddress(model.Address address) {
    var stateCtx = state?.context;
    user_repo.updateAddress(address).then((value) {
      setState(() {});
      addresses.clear();
      listenForAddresses(message: AppLocalizations.of(stateCtx!)!.the_address_updated_successfully);
    });
  }

  void removeBillingAddress(model.Address address) async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    user_repo.removeDeliveryAddress(address).then((value) {
      setState(() {
        this.addresses.remove(address);
      });
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(stateCtx!)!.billing_address_removed_successfully),
      ));
    });
  }
}
