import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:gogo_online_ios/src/models/cart.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:webview_flutter/webview_flutter.dart';
import '../repository/user_repository.dart' as user_repo;

class NetCashController extends ControllerMVC{
  late GlobalKey<ScaffoldState> scaffoldKey;
  late  WebViewController webViewController;
  late List<Cart> currentCarts;
  late double total;
  String url = "";

  static NetCashController? _instance;
  factory NetCashController() => _instance ??= NetCashController._();

  NetCashController._(){
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  @override
  void initState() {
    final String _apiToken = 'api_token=${user_repo.currentUser.value.apiToken}';
    setState(() {});
    super.initState();
  }

///netcash?doctor=${getDoctor()}&consultation_date=${getDate()&total=$total&consultation_number=${getTransId()}&email=${getUserEmail()}&cellphone=${getUserCellphone()}
  String getPaymentUrl(){
   String payUrl = '${GlobalConfiguration().getValue('base_url')}netcash?doctor=${getDoctor()}&consultation_date=${getDate()}&total=$total&consultation_number=${getTransId()}&email=${getUserEmail()}&cellphone=${getUserCellphone()}';
    print(payUrl);
    return payUrl;
  }

  String getDoctor()=>currentCarts[0].product.healer.name;

  String getDate()=>currentCarts[0].consultationDate;

  String getUserEmail() =>  user_repo.currentUser.value.email??"";

  String getUserCellphone()=> user_repo.currentUser.value.phone??"";

  String getTransId() => currentCarts[0].id.toString();

}