import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../models/cart.dart';
import '../models/favorite.dart';
import '../models/option.dart';
import '../models/product.dart';
import '../repository/cart_repository.dart';
import '../repository/product_repository.dart';

class ProductController extends ControllerMVC {
  late Product product;
  double quantity = 1;
  double total = 0;
  List<Cart> carts = [];
 late Favorite favorite;
  bool loadCart = false;
  late GlobalKey<ScaffoldState> scaffoldKey;

  ProductController() {
    scaffoldKey = GlobalKey<ScaffoldState>();
    product = Product.fromJSON({});
    favorite = Favorite.fromJSON({});
  }

  void listenForProduct({required String productId, String? message}) async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    final Stream<Product> stream = await getProduct(productId);
    stream.listen((Product prod) {
      setState(() => product = prod);
    }, onError: (a) {
      print(a);
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(
            AppLocalizations.of(stateCtx!)!.verify_your_internet_connection),
      ));
    }, onDone: () {
      calculateTotal();
      if (message != null) {
        ScaffoldMessenger.of(scaffoldCtx!)
            .showSnackBar(SnackBar(content: Text(message)));
      }
    });
  }

  void listenForFavorite(String productId) async {
    final Stream<Favorite> stream = await isFavoriteProduct(productId);
    stream.listen((Favorite fav) {
      setState(() => favorite = fav);
    }, onError: (a) {
      print(a);
    });
  }

  void listenForCart() async {
    final Stream<Cart> stream = await getCart();
    stream.listen((Cart _cart) {
      carts.add(_cart);
    });
  }

  bool isSameMarkets(Product product) {
    if (carts.isNotEmpty) {
      return carts[0].product.healer.id == product.healer.id;
    }
    return true;
  }

  void addToCart(Product product, {bool reset = false}) async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    setState(() {
      loadCart = true;
    });

    var newCart =  Cart();
    newCart.product = product;
    newCart.options = product.options.where((element) => element.checked).toList();
    newCart.quantity = quantity;
    // if product exist in the cart then increment quantity
    var oldCart = isExistInCart(newCart);
    if (carts.isNotEmpty) {
      //_oldCart.quantity += this.quantity;
     // oldCart.id??="";
      oldCart.quantity =1;
      updateCart(oldCart).then((value) {
        setState(() {
          loadCart = false;
        });
      }).whenComplete(() {
        ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
          content: Text(AppLocalizations.of(stateCtx!)!.this_consultation_was_added_to_wait_room),
        ));
      });
    } else {
      // the product doesnt exist in the cart add new one
      addCart(newCart, reset).then((value) {
        setState(() {
          loadCart = false;
        });
      }).whenComplete(() {
        ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
          content: Text(AppLocalizations.of(stateCtx!)!.this_consultation_was_added_to_wait_room),
        ));
      });
    }
  }

  Cart isExistInCart(Cart cart) {
    return carts.firstWhere((Cart oldCart) => cart.isSame(oldCart), orElse: () => Cart.fromJSON({}));
  }



  void addToFavorite(Product product) async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    var _favorite =  Favorite();
    _favorite.product = product;
    _favorite.options = product.options.where((Option _option) {
      return _option.checked;
    }).toList();
    addFavorite(_favorite).then((value) {
      setState(() {
        favorite = value;
      });
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(stateCtx!)!.thisProductWasAddedToFavorite),
      ));
    });
  }

  void removeFromFavorite(Favorite _favorite) async {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    removeFavorite(_favorite).then((value) {
      setState(() {
        favorite =  Favorite();
      });
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(stateCtx!)!.thisProductWasRemovedFromFavorites),
      ));
    });
  }

  Future<void> refreshProduct() async {
    var stateCtx = state?.context;
    var id = product.id;
    product =  Product.fromJSON({});
    listenForFavorite(id);
    listenForProduct(productId: id, message: AppLocalizations.of(stateCtx!)!.productRefreshedSuccessfuly);
  }

  void calculateTotal() {
    total = product.price ?? 0;
    for (var option in product.options) {
      total += option.checked ? option.price : 0;
    }
    total *= quantity;
    setState(() {});
  }

  incrementQuantity() {
    if (quantity <= 99) {
      ++quantity;
      calculateTotal();
    }
  }

  decrementQuantity() {
    if (quantity > 1) {
      --quantity;
      calculateTotal();
    }
  }
}
