import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../helpers/helper.dart';
import '../models/category.dart';
import '../models/healer.dart';
import '../models/product.dart';
import '../models/review.dart';
import '../repository/category_repository.dart';
import '../repository/healer_repository.dart';
import '../repository/product_repository.dart';
import '../repository/settings_repository.dart' as setting_repo;
import '../models/address.dart' as model;
import '../models/filter.dart';
import '../repository/settings_repository.dart';

class HomeController extends ControllerMVC {
  List<model.Address> addresses = <model.Address>[];
  List<Category> categories = <Category>[];
  List<Healer> topHealers = <Healer>[];
  List<Healer> popularHealers = <Healer>[];
  List<Healer> allHealers = <Healer>[];
  List<Review> recentReviews = <Review>[];
  List<Product> trendingProducts = <Product>[];
  String currentCity = "";

  static HomeController? _instance;
  factory HomeController() => _instance ??= HomeController._();

  HomeController._() {
    listenForTopHealers();
   // listenForTrendingProducts();
    //listenForCategories();
    listenForAddresses();
    listenForPopularHealers();
    listenForAllHealers();
    listenForRecentReviews();

  }

  Future<void> listenForCategories() async {
    final Stream<Category> stream = await getCategories();
    stream.listen((Category category) {
      setState(() => categories.add(category));
    }, onError: (a) {
      print(a);
    }, onDone: () {});
  }

  Future<void> listenForTopHealers() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    Filter filter = Filter.fromJSON(json.decode(prefs.getString('filter') ?? '{}'));

    final Stream<Healer> stream = await getNearHealers(billingAddress.value, billingAddress.value);
    stream.listen((Healer healer) {
      setState(() => topHealers.add(healer));
    }, onError: (a) {}, onDone: () {
      if(filter.isCurrentCity){
        var filterCities = filterHealerByCity(topHealers, currentCity);  //check count on done
        setState(() {
          topHealers = filterCities;
        });
      }
    });
  }

  Future<void> listenForPopularHealers() async {
    final Stream<Healer> stream = await getPopularHealers(billingAddress.value);
    stream.listen((Healer healer) {
      setState(() => popularHealers.add(healer));
    }, onError: (a) {}, onDone: () {});
  }

  Future<void> listenForAllHealers() async {
    final Stream<Healer> stream = await getAllHealers(billingAddress.value);
    stream.listen((Healer healer) {
      setState(() => allHealers.add(healer));
    }, onError: (a) {}, onDone: () {});
  }

  Future<void> listenForRecentReviews() async {
    final Stream<Review> stream = await getRecentReviews();
    stream.listen((Review review) {
      setState(() => recentReviews.add(review));
    }, onError: (a) {}, onDone: () {});
  }

  Future<void> listenForTrendingProducts() async {
    final Stream<Product> stream = await getTrendingProducts(billingAddress.value);
    stream.listen((Product product) {
      setState(() => trendingProducts.add(product));
    }, onError: (a) {
      print(a);
    }, onDone: () {});
  }

  void requestForCurrentLocation(BuildContext context) {
    OverlayEntry loader = Helper.overlayLoader(context);
    Overlay.of(context)?.insert(loader);
    setCurrentLocation().then((address) async {
      billingAddress.value = address;
      await refreshHome();
      loader.remove();
    }).catchError((e) {
      loader.remove();
    });
  }

  Future<void> refreshHome() async {
    setState(() {
      categories = <Category>[];
      topHealers = <Healer>[];
      popularHealers = <Healer>[];
      recentReviews = <Review>[];
      trendingProducts = <Product>[];
    });
    await listenForTopHealers();
   // await listenForTrendingProducts();
   // await listenForCategories();
    await listenForPopularHealers();
    await listenForRecentReviews();
  }

  void listenForAddresses({String? message}) async {
    var stateCtx = state?.context;
    setting_repo.getCurrentLocation().then((value) {
      setState(() {
        addresses.add(value);
        currentCity = value.city!;
        print("Set city in home controller ${value.city}");
      });
    });
  }


  String extractCity(String filterAddress) {
    const startChr = '*';
    const endChr = '#';

    final startIndex = filterAddress.indexOf(startChr);
    final endIndex = filterAddress.indexOf(endChr);
    var city ="";
    try{
      city = filterAddress.substring(startIndex + startChr.length, endIndex);
    }catch (e){
      print(e);
    }
   // var city = filterAddress.substring(startIndex + startChr.length, endIndex);

    return city;//filterAddress.substring(startIndex + startChr.length, endIndex);
  }

  List<Healer> filterHealerByCity(List<Healer> toFilterHealers, String city){
    List<Healer> filteredHealerList = <Healer>[];
    for(Healer healer in toFilterHealers){
        var healerCity = extractCity(healer.address);
        if(healerCity.toLowerCase().trim() == city.toLowerCase().trim()){
            filteredHealerList.add(healer);
        }
    }
    return filteredHealerList;
  }

}


/*void main() {
  const str = "the quick brown fox jumps over the lazy dog";
  const start = "quick";
  const end = "over";

  final startIndex = str.indexOf(start);
  final endIndex = str.indexOf(end, startIndex + start.length);

  print(str.substring(startIndex + start.length, endIndex)); // brown fox jumps
}*/