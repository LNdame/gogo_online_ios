
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../repository/settings_repository.dart' as setting_repo;
import '../repository/user_repository.dart' as user_repo;

class AppController extends ControllerMVC {//with AppController
  factory AppController() => _this ??= AppController._();
  AppController._();
  static AppController? _this;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setting_repo.initSettings();
   // settingRepo.getCurrentLocation();
    user_repo.getCurrentUser();
  }

  @override
  Future<bool> initAsync() async {
    setting_repo.initSettings();
    // settingRepo.getCurrentLocation();
    user_repo.getCurrentUser();
    return true;

  }

  @override
  bool onAsyncError(FlutterErrorDetails details) {
    return false;
  }
  
}