import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
//import 'package:gogo_online/src/controllers/netcash_controller.dart';
import 'package:gogo_online_ios/src/models/cart.dart';
import 'package:gogo_online_ios/src/pages/netcash_payment.dart';
//import 'package:gogo_online/src/pages/paystack_payment.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../models/address.dart' as model;
import '../models/payment_method.dart';
import '../repository/settings_repository.dart' as setting_repo;
import '../repository/user_repository.dart' as user_repo;
import 'cart_controller.dart';

class ConsultationSummaryController extends CartController {
 late GlobalKey<ScaffoldState> scaffoldKey;
 late  model.Address deliveryAddress;
  PaymentMethodList? list;

  //static ConsultationSummaryController? _instance;
  //factory ConsultationSummaryController()=> _instance ??=ConsultationSummaryController._();

  ConsultationSummaryController() {
    scaffoldKey =  GlobalKey<ScaffoldState>();
    super.listenForCarts();
    listenForDeliveryAddress();
    if (kDebugMode) {
     // print(setting_repo.billingAddress.value.toMap());
    }
  }

  void listenForDeliveryAddress() async {
    deliveryAddress = setting_repo.billingAddress.value;
  }

  void addAddress(model.Address address) {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    user_repo.addAddress(address).then((value) {
      setState(() {
        setting_repo.billingAddress.value = value;
        deliveryAddress = value;
      });
    }).whenComplete(() {
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(stateCtx!)!.new_address_added_successfully),
      ));
    });
  }

  void updateAddress(model.Address address) {
    var stateCtx = state?.context;
    var scaffoldCtx = scaffoldKey.currentContext;
    user_repo.updateAddress(address).then((value) {
      setState(() {
        setting_repo.billingAddress.value = value;
        this.deliveryAddress = value;
      });
    }).whenComplete(() {
      ScaffoldMessenger.of(scaffoldCtx!).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(stateCtx!)!.the_address_updated_successfully),
      ));
    });
  }

  PaymentMethod getPickUpMethod() {
    return list!.pickupList.elementAt(0);
  }

  PaymentMethod getDeliveryMethod() {
    return list!.pickupList.elementAt(1);
  }

  void toggleDelivery() {
    list!.pickupList.forEach((element) {
      if (element != getDeliveryMethod()) {
        element.selected = false;
      }
    });
    setState(() {
      getDeliveryMethod().selected = !getDeliveryMethod().selected;
    });
  }

  void togglePickUp() {
    for (var element in list!.pickupList) {
      if (element != getPickUpMethod()) {
        element.selected = false;
      }
    }
    setState(() {
      getPickUpMethod().selected = !getPickUpMethod().selected;
    });
  }

  PaymentMethod getSelectedMethod() {
    return list!.pickupList.firstWhere((element) => element.selected);
  }

  @override
  void goCheckout(BuildContext context) {

    Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => NetCashPaymentWidget(carts: carts, total: total,))
    );

  }
}
