import 'package:flutter/material.dart';
import 'package:gogo_online_ios/src/utils/ThemeUtil.dart';

class BlockButtonWidget extends StatelessWidget {
  const BlockButtonWidget({Key? key, required this.color, required this.text, required this.onPressed})
      : super(key: key);

  final Color color;
  final Text text;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(color: color.withOpacity(0.4), blurRadius: 40, offset: const Offset(0, 15)),
          BoxShadow(color: color.withOpacity(0.4), blurRadius: 13, offset: const Offset(0, 3))
        ],
        borderRadius: const BorderRadius.all(Radius.circular(100)),
      ),
      child: TextButton(
        style:  ThemeUtil.getCustomButtonStyle(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 14),
            context: context,
            backgroundColor: color),
        onPressed: onPressed,
        child: text,
      ),
    );
  }
}
