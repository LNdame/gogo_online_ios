import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:gogo_online_ios/src/controllers/consultation_item_controller.dart';
import 'package:gogo_online_ios/src/helpers/app_constants.dart';
import 'package:gogo_online_ios/src/helpers/helper.dart';
import 'package:gogo_online_ios/src/models/chat_data.dart';
import 'package:gogo_online_ios/src/models/consultation.dart';
import 'package:gogo_online_ios/src/models/route_argument.dart';
import 'package:gogo_online_ios/src/pages/messaging/messaging_screen.dart';
import 'package:gogo_online_ios/src/repository/user_repository.dart';
import 'package:gogo_online_ios/src/utils/ValidatorUtil.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'ProductOrderItemWidget.dart';


class ConsultationItemWithDateWidget extends StatefulWidget {
   bool? expanded = false;
  late  Consultation consultation;
  final ValueChanged<void>? onCanceled;

   ConsultationItemWithDateWidget({Key? key, this.expanded, required this.consultation, this.onCanceled}) : super(key: key);

  @override
  State createState() => _ConsultationItemWithDateWidgetState();
}

class _ConsultationItemWithDateWidgetState extends StateMVC<ConsultationItemWithDateWidget> {
  late ConsultationItemController _con;
  bool showChatButton = false;

  _ConsultationItemWithDateWidgetState(): super(ConsultationItemController()){
    _con= controller as ConsultationItemController;
  }

  @override
  void initState() {

    var id = widget.consultation.productConsultations[0].product.healer.id;

    if (currentUser.value.role?.name ==AppConstants.ROLE_MANAGER){
      _con.listenForPatient(clUser: widget.consultation.user, currentUserUid: currentUser.value.firebaseUid??"" );
    }else{
      _con.listenForHealer(id: id, currentUserUid: currentUser.value.firebaseUid??"" );
    }

    showChatButton = ValidatorUtil.isNowPastTheDate(widget.consultation.consultationDate, widget.consultation.consultationStartTime);
    
    super.initState();
  }

  void goToChatClient(BuildContext context){
    final initData = _con.chatData ??=
         ChatData(
            groupId: _con.getGroupId(currentUser.value.firebaseUid??""),
            userId: currentUser.value.firebaseUid??"",
            peerId: _con.healerPeer.id,
            peer: _con.healerPeer,
            messages: []
        );
    Navigator.of(context).push(MaterialPageRoute(builder: (context)=> MessagingScreenWidget(chatData: initData)));
  }

  void goToChatHealer(BuildContext context){
    final initData = _con.chatData ??=
         ChatData(
            groupId: _con.getGroupId(currentUser.value.firebaseUid??""),
            userId: currentUser.value.firebaseUid??"",
            peerId: _con.clientPeer.id,
            peer: _con.clientPeer,
            messages: []
        );
    Navigator.of(context).push(MaterialPageRoute(builder: (context)=> MessagingScreenWidget(chatData: initData)));
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    return Container(
      height: 400,
      child: Stack(
        children: <Widget>[
          Opacity(
            opacity: widget.consultation.active ? 1 : 0.4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 14),
                  padding: EdgeInsets.only(top: 20, bottom: 5),
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor.withOpacity(0.9),
                    boxShadow: [
                      BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.1), blurRadius: 5, offset: Offset(0, 2)),
                    ],
                  ),
                  child: Theme(
                    data: theme,
                    child: ExpansionTile(
                      initiallyExpanded: widget.expanded!,
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('Date: ${widget.consultation.consultationDate}'),
                          Text('Time: ${widget.consultation.consultationStartTime}',
                        // DateFormat('dd-MM-yyyy | HH:mm').format(widget.consultation.dateTime),
                        style: Theme.of(context).textTheme.caption,),
                        ],
                      ),
                      trailing: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Helper.getPrice(Helper.getTotalOrdersPrice(widget.consultation), context, style: Theme.of(context).textTheme.headline4),
                          Text('Consultation No: #${widget.consultation.id}',
                            style: Theme.of(context).textTheme.caption,
                          )
                        ],
                      ),
                      children: <Widget>[
                        Column(
                            children: List.generate(
                              widget.consultation.productConsultations.length,
                                  (indexProduct) {
                                return ProductOrderItemWidget(
                                    heroTag: 'mywidget.orders', consultation: widget.consultation, productConsultation: widget.consultation.productConsultations.elementAt(indexProduct));
                              },
                            )),
                        showChatButton?
                        Padding(
                                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                child: TextButton.icon(
                                  onPressed: () {
                                    if (currentUser.value.role?.name == AppConstants.ROLE_MANAGER) {
                                      goToChatHealer(context);
                                    } else {
                                      goToChatClient(context);
                                    }
                                  },
                                  style: TextButton.styleFrom(
                                      primary: Theme.of(context).primaryColor,
                                      backgroundColor: Theme.of(context).colorScheme.secondary,
                                      textStyle: Theme.of(context)
                                          .textTheme
                                          .caption!
                                          .merge(TextStyle(height: 1, color: Theme.of(context).primaryColor)),
                                      shape: StadiumBorder(),
                                      elevation: 5),
                                  icon: Icon(
                                    Icons.chat,
                                    color: Theme.of(context).primaryColor,
                                  ),
                                  label: Text(
                                    currentUser.value.role?.name == AppConstants.ROLE_MANAGER
                                        ? "Chat with client"
                                        : "Chat with healer",
                                    textAlign: TextAlign.start,
                                  ),
                                ),
                              )
                            : SizedBox(
                                height: 0,
                              ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Text(
                                      currentUser.value.role?.name == AppConstants.ROLE_MANAGER
                                          ? widget.consultation.user.name??""
                                          : "",
                                      style:
                                          Theme.of(context).textTheme.bodyText1,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Text(
                                      '${AppLocalizations.of(context)!.tax} (${widget.consultation.tax}%)',
                                      style: Theme.of(context).textTheme.bodyText1,
                                    ),
                                  ),
                                  Helper.getPrice(Helper.getTaxOrder(widget.consultation), context, style: Theme.of(context).textTheme.subtitle1)
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Text(
                                      AppLocalizations.of(context)!.total,
                                      style: Theme.of(context).textTheme.bodyText1,
                                    ),
                                  ),
                                  Helper.getPrice(Helper.getTotalOrdersPrice(widget.consultation), context, style: Theme.of(context).textTheme.headline4)
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Wrap(
                    alignment: WrapAlignment.end,
                    children: <Widget>[
                   /*   FlatButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed('/Tracking', arguments: RouteArgument(id: widget.consultation.id));
                        },
                        textColor: Theme.of(context).hintColor,
                        child: Wrap(
                          children: <Widget>[Text(S.of(context).view)],
                        ),
                        padding: EdgeInsets.symmetric(horizontal: 0),
                      ), */
                      if (widget.consultation.canCancelConsultation())
                        TextButton(
                          onPressed: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                // return object of type Dialog
                                return AlertDialog(
                                  title: Wrap(
                                    spacing: 10,
                                    children: <Widget>[
                                      Icon(Icons.report, color: Colors.orange),
                                      Text(
                                        AppLocalizations.of(context)!.confirmation,
                                        style: TextStyle(color: Colors.orange),
                                      ),
                                    ],
                                  ),
                                  content: Text(AppLocalizations.of(context)!.areYouSureYouWantToCancelThisOrder),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 30, vertical: 25),
                                  actions: <Widget>[
                                    TextButton(
                                      child:  Text(
                                        AppLocalizations.of(context)!.yes,
                                        style: TextStyle(color: Theme.of(context).hintColor),
                                      ),
                                      onPressed: () {
                                        widget.onCanceled!(widget.consultation);
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                    TextButton(
                                      child: Text(
                                        AppLocalizations.of(context)!.close,
                                        style: TextStyle(color: Colors.orange),
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                          style: TextButton.styleFrom(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          foregroundColor: Theme.of(context).hintColor,
                          ),
                          child: Wrap(
                            children: <Widget>[Text("${AppLocalizations.of(context)!.cancel} ")],
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsetsDirectional.only(start: 20),
            padding: EdgeInsets.symmetric(horizontal: 10),
            height: 28,
            width: 140,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(100)), color: widget.consultation.active ? Theme.of(context).colorScheme.secondary : Colors.redAccent),
            alignment: AlignmentDirectional.center,
            child: Text(
              widget.consultation.active ? widget.consultation.consultationStatus.status : AppLocalizations.of(context)!.canceled,
              maxLines: 1,
              overflow: TextOverflow.fade,
              softWrap: false,
              style: Theme.of(context).textTheme.caption!.merge(TextStyle(height: 1, color: Theme.of(context).primaryColor)),
            ),
          ),
        ],
      ),
    );
  }
}
