import 'package:flutter/material.dart';
import 'package:gogo_online_ios/src/elements/CardsVertCarouselLoaderWidget.dart';
import 'package:gogo_online_ios/src/models/healer.dart';
import 'package:gogo_online_ios/src/models/route_argument.dart';

import 'CardWidget.dart';

// ignore: must_be_immutable
class CardsVerticalCarouselWidget extends StatefulWidget {
  List<Healer> healersList;
  String? heroTag;

  CardsVerticalCarouselWidget({Key? key, required this.healersList, this.heroTag}) : super(key: key);

  @override
  State createState() => _CardsVerticalCarouselWidgetState();
}

class _CardsVerticalCarouselWidgetState extends State<CardsVerticalCarouselWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.healersList.isEmpty
        ? CardsVertCarouselLoaderWidget()
        : Container(
            child: ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemCount: widget.healersList.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed('/Details',
                          arguments: RouteArgument(
                            id: widget.healersList.elementAt(index).id,
                            heroTag: widget.heroTag,
                          ));
                    },
                    child: CardWidget(healer: widget.healersList.elementAt(index), heroTag: widget.heroTag),
                  );
                }),
          );
  }
}
