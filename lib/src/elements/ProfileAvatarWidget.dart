
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gogo_online_ios/route_constants.dart';
import 'package:gogo_online_ios/src/repository/services/storage.dart';
import '../helpers/app_constants.dart';
import '../models/user.dart';

class ProfileAvatarWidget extends StatelessWidget {
  final User user;

  ProfileAvatarWidget({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var storage = Storage();
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 30),
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.secondary,
        borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30)),
      ),
      child: Column(
        children: <Widget>[
          Container(
            height: 160,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Stack(children: [
                   ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                    child: FutureBuilder<String>(
                      future: storage.getUrl(AppConstants.healerProfilePath, "${user.firebaseUid??""}.jpg"),
                      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                        if(snapshot.hasData){
                          return CachedNetworkImage(imageUrl: snapshot.data!,
                            placeholder: (context, url) => Image.asset(
                              'assets/img/loading.gif',
                              fit: BoxFit.cover,
                              height: 135,
                              width: 135,
                            ),
                            errorWidget: (context, url, error) => Icon(Icons.error),
                          );
                        } else {
                         return CachedNetworkImage(
                              height: 155,
                              width: 255,
                              fit: BoxFit.cover,
                              imageUrl:    user.image?.url ?? "",
                        placeholder: (context, url) => Image.asset(
                        'assets/img/loading.gif',
                        fit: BoxFit.cover,
                        height: 135,
                        width: 135,
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),);
                         }
                        }
                       ,
                    )
                  ),
                  Positioned(
                      bottom: 0,
                      right: 0,
                      child: ElevatedButton.icon(
                          onPressed: () {
                            Navigator.of(context).pushNamed(setPhotoScreenRoute, arguments: user);
                          },
                          style: ElevatedButton.styleFrom(backgroundColor: Theme.of(context).focusColor),
                          icon: Icon(
                            Icons.camera_alt_rounded,
                            size: 28,
                            color: Theme.of(context).primaryColor,
                          ),
                          label: Text("")))
                ]),
              ],
            ),
          ),
          Text(
            user.name ?? "",
            style: Theme.of(context).textTheme.headlineSmall!.merge(TextStyle(color: Theme.of(context).primaryColor)),
          ),
          Text(
            user.address ?? "",
            style: Theme.of(context).textTheme.bodySmall!.merge(TextStyle(color: Theme.of(context).primaryColor)),
          ),
        ],
      ),
    );
  }
}
