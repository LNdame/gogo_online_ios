import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../controllers/cart_controller.dart';
import '../helpers/helper.dart';

class WaitingRoomBottomDetailsWidget extends StatelessWidget {
  const WaitingRoomBottomDetailsWidget({
    Key? key,
    required CartController con,
  })  : _con = con,
        super(key: key);

  final CartController _con;

  @override
  Widget build(BuildContext context) {
    return _con.carts.isEmpty
        ? SizedBox(height: 0)
        : Container(
            height: 200,
            padding:const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: const BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
                boxShadow: [BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.15), offset: Offset(0, -2), blurRadius: 5.0)]),
            child: SizedBox(
              width: MediaQuery.of(context).size.width - 40,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          AppLocalizations.of(context)!.subtotal,
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                      Helper.getPrice(_con.subTotal, context, style: Theme.of(context).textTheme.subtitle1, zeroPlaceholder: '0')
                    ],
                  ),
                  SizedBox(height: 5),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text("Additional fee",
                        //  AppLocalizations.of(context)!.additional_fee,
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                   //   if (Helper.canDelivery(_con.carts[0].product.healer, carts: _con.carts))
                        if (true)
                        //Helper.getPrice(_con.carts[0].product.healer.deliveryFee, context, style: Theme.of(context).textTheme.subtitle1, zeroPlaceholder: 'Free')
                          Helper.getPrice(0, context, style: Theme.of(context).textTheme.subtitle1, zeroPlaceholder: 'Free')
                      else
                        Helper.getPrice(0, context, style: Theme.of(context).textTheme.subtitle1, zeroPlaceholder: 'Free')
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          '${AppLocalizations.of(context)!.tax} (${_con.carts[0].product.healer.defaultTax}%)',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                      Helper.getPrice(_con.taxAmount, context, style: Theme.of(context).textTheme.subtitle1)
                    ],
                  ),
                  SizedBox(height: 10),
                  Stack(
                    fit: StackFit.loose,
                    alignment: AlignmentDirectional.centerEnd,
                    children: <Widget>[
                      SizedBox(
                        width: MediaQuery.of(context).size.width - 40,
                        child: TextButton(
                          onPressed: _con.carts.first.consultationDate.isNotEmpty? () {
                            _con.goCheckout(context);
                          }:null,
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.symmetric(vertical: 14),
                            shape:  StadiumBorder(),
                            foregroundColor: !_con.carts[0].product.healer.closed ? Theme.of(context).colorScheme.secondary : Theme.of(context).focusColor.withOpacity(0.5),
                            disabledForegroundColor: Theme.of(context).focusColor.withOpacity(0.5),
                          ),

                          child: Text(
                            AppLocalizations.of(context)!.checkout,
                            textAlign: TextAlign.start,
                            style: Theme.of(context).textTheme.bodyText1!.merge(TextStyle(color: Theme.of(context).primaryColor)),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Helper.getPrice(_con.total, context,
                            style: Theme.of(context).textTheme.headline4!.merge(TextStyle(color: Theme.of(context).primaryColor)), zeroPlaceholder: 'Free'),
                      )
                    ],
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            ),
          );
  }
}
