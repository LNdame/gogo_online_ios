import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:gogo_online_ios/src/helpers/app_constants.dart';
import 'package:url_launcher/url_launcher.dart';
import '../helpers/helper.dart';
import '../models/product.dart';

class ProductItemWidget extends StatelessWidget {
  final String? heroTag;
  final Product product;

  const ProductItemWidget({Key? key, required this.product, this.heroTag}) : super(key: key);

  String wUrl() {
    if (defaultTargetPlatform == TargetPlatform.android) {
      return "https://wa.me/${AppConstants.gogoPhoneNumber}/?text=${Uri.encodeFull(composeMsg(product))}";
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      return "whatsapp://wa.me/${AppConstants.gogoPhoneNumber}/?text=${Uri.encodeFull(composeMsg(product))}";
    } else {
      return "https://wa.me/${AppConstants.gogoPhoneNumber}/?text=${Uri.parse(composeMsg(product))}";
    }
  }

  String composeMsg(Product product){
    return "${AppConstants.gogoContactMessage} with ${product.healer.name} [Gogo ID: ${product.healer.id}]";
  }

  void _launchWhatsApp() async {
    if (await canLaunch(wUrl())) {
      await launch(wUrl());
    } else {
      throw 'Could not launch whatsapp';
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Theme.of(context).colorScheme.secondary,
      focusColor: Theme.of(context).colorScheme.secondary,
      highlightColor: Theme.of(context).primaryColor,
      onTap: () async {
        _launchWhatsApp();

        // Navigator.of(context).pushNamed(productRoute, arguments: RouteArgument(id: product.id, heroTag: this.heroTag));
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor.withOpacity(0.9),
            boxShadow: [
              BoxShadow(
                  color: Theme.of(context).focusColor.withOpacity(0.1),
                  blurRadius: 5,
                  offset: Offset(0, 2),
                  spreadRadius: 5),
            ],
            borderRadius: BorderRadius.circular(16)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Hero(
              tag: "$heroTag${product.id}",
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                child:Image.asset("assets/img/whatsapp.png",
                  fit: BoxFit.cover,
                  height: 60,
                  width: 60,)
               /* CachedNetworkImage(
                  height: 60,
                  width: 60,
                  fit: BoxFit.cover,
                  imageUrl: product.image.thumb ?? "",
                  placeholder: (context, url) => Image.asset(
                    'assets/img/loading.gif',
                    fit: BoxFit.cover,
                    height: 60,
                    width: 60,
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),*/
              ),
            ),
            SizedBox(width: 15),
            Flexible(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text( "Contact me for a booking",
                         // product.name,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                       /* Row(
                            //children: Helper.getStarsList(product.getRate()),
                            children: [
                              Text(
                                product.unit,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                style: Theme.of(context).textTheme.bodyLarge,
                              )
                            ]),
                        Text(
                          product.options.map((e) => e.name).toList().join(', '),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.bodySmall,
                        ),*/
                      ],
                    ),
                  ),
                  SizedBox(width: 8),
                  //this column  display the price reactivate if needed
                 /* Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Helper.getPrice(
                        product.price,
                        context,
                        style: Theme.of(context).textTheme.headlineMedium,
                      ),
                      product.discountPrice > 0
                          ? Helper.getPrice(product.discountPrice, context,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .merge(TextStyle(decoration: TextDecoration.lineThrough)))
                          : SizedBox(height: 0),
                    ],
                  ),*/
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
