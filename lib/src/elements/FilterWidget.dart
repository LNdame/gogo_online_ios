import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gogo_online_ios/src/helpers/app_constants.dart';
import 'package:gogo_online_ios/src/utils/ThemeUtil.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../../route_constants.dart';
import '../controllers/filter_controller.dart';
import '../elements/CircularLoadingWidget.dart';
import '../models/filter.dart';

class FilterWidget extends StatefulWidget {
  final ValueChanged<Filter>? onFilter;

   FilterWidget({Key? key, this.onFilter}) : super(key: key);

  @override
  State createState() => _FilterWidgetState();
}

class _FilterWidgetState extends StateMVC<FilterWidget> {
 late FilterController _con;

  _FilterWidgetState() : super(FilterController()) {
    _con = controller as FilterController ;
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SafeArea(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(AppLocalizations.of(context)!.filter),
                  MaterialButton(
                    onPressed: () {
                      _con.clearFilter();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.clear,
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: ListView(
                primary: true,
                shrinkWrap: true,
                children: <Widget>[
                  ExpansionTile(
                    title:Text("Provinces"),
                    initiallyExpanded: true, //  Text(S.of(context).delivery_or_pickup),
                    children: List.generate(_con.provinces.length, (index)  {
                      return CheckboxListTile(
                      controlAffinity: ListTileControlAffinity.trailing,
                      value: _con.provinces.elementAt(index).selected,
                      onChanged: (value) {
                        _con.onChangeProvincesFilter(index);
                      },
                      title: Text(
                        _con.provinces.elementAt(index).name??"",
                        overflow: TextOverflow.fade,
                        softWrap: false,
                        maxLines: 1,
                      ),
                    );
                    }),
                  ),
                  ExpansionTile(
                    title: Text(AppLocalizations.of(context)!.city),
                    initiallyExpanded: true,
                    children: [
                      CheckboxListTile(
                        controlAffinity: ListTileControlAffinity.trailing,
                        value: _con.filter?.isCurrentCity ?? false,
                        onChanged: (value) {
                          setState(() {
                            _con.filter?.isCurrentCity = value!;
                          });
                        },
                        title: Text(AppLocalizations.of(context)!.current_location,
                         // S.of(context).open,
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          maxLines: 1,
                        ),
                      ),
                    ],
                  ),
                 /* _con.fields.isEmpty
                      ? CircularLoadingWidget(height: 100)
                      : ExpansionTile(
                          title: Text(AppLocalizations.of(context)!.fields),
                          initiallyExpanded: true,
                          children: List.generate(_con.fields.length, (index) {
                            return CheckboxListTile(
                              controlAffinity: ListTileControlAffinity.trailing,
                              value: _con.fields.elementAt(index).selected,
                              onChanged: (value) {
                                _con.onChangeFieldsFilter(index);
                              },
                              title: Text(
                                _con.fields.elementAt(index).name,
                                overflow: TextOverflow.fade,
                                softWrap: false,
                                maxLines: 1,
                              ),
                            );
                          }),
                        ), */
                ],
              ),
            ),
            SizedBox(height: 15),
            TextButton(
              onPressed: () {
                _con.saveFilter().whenComplete(() {
                  setState(() {
                    widget.onFilter!(_con.filter);
                    Navigator.of(context).pushNamed(pagesRoute, arguments: homeIndex);
                  });
                });
              },
              style: ThemeUtil.getCustomButtonStyle(
                  padding:  EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                  context: context,
                  backgroundColor: Theme.of(context).colorScheme.secondary,),

              child: Text(
                AppLocalizations.of(context)!.apply_filters,
                textAlign: TextAlign.start,
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
            ),
            SizedBox(height: 15)
          ],
        ),
      ),
    );
  }
}
