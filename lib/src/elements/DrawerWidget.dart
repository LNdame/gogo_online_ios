import 'package:flutter/material.dart';
import 'package:gogo_online_ios/route_constants.dart';
import 'package:gogo_online_ios/src/helpers/app_constants.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../controllers/profile_controller.dart';
import '../repository/settings_repository.dart';
import '../repository/user_repository.dart';

class DrawerWidget extends StatefulWidget {
  const DrawerWidget({Key? key}) : super(key: key);

  @override
  State createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends StateMVC<DrawerWidget> {
  _DrawerWidgetState() : super(ProfileController());

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              currentUser.value.apiToken != null
                  ? Navigator.of(context).pushNamed('/Profile')
                  : Navigator.of(context).pushNamed(loginRoute);
            },
            child: currentUser.value.apiToken != null
                ? UserAccountsDrawerHeader(
                    decoration: BoxDecoration(
                      color: Theme.of(context).hintColor.withOpacity(0.1),
                    ),
                    accountName: Text(
                      currentUser.value.name ?? "",
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    accountEmail: Text(
                      currentUser.value.email ?? "",
                      style: Theme.of(context).textTheme.bodySmall,
                    ),
                    currentAccountPicture: CircleAvatar(
                      backgroundColor: Theme.of(context).colorScheme.secondary,
                      backgroundImage: NetworkImage(currentUser.value.image?.thumb ?? ""),
                    ),
                  )
                : Container(
                    padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 15),
                    decoration: BoxDecoration(
                      color: Theme.of(context).hintColor.withOpacity(0.1),
                    ),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.person,
                          size: 32,
                          color: Theme.of(context).colorScheme.secondary.withOpacity(1),
                        ),
                        const SizedBox(width: 30),
                        Text(
                          AppLocalizations.of(context)!.guest,
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                      ],
                    ),
                  ),
          ),
          ListTile(
            onTap: () {
              currentUser.value.role?.name == AppConstants.ROLE_CLIENT
                  ? Navigator.of(context).pushNamed(pagesRoute, arguments: homeIndex)
                  : Navigator.of(context).pushNamed(healerPagesRoute, arguments: homeIndex);
            },
            leading: Icon(
              Icons.home,
              color: Theme.of(context).focusColor.withOpacity(1),
            ),
            title: Text(
              AppLocalizations.of(context)!.home,
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ),
          ListTile(
            onTap: () {
              currentUser.value.role?.name == AppConstants.ROLE_CLIENT
                  ? Navigator.of(context).pushNamed(pagesRoute, arguments: notificationIndex)
                  : Navigator.of(context).pushNamed(healerPagesRoute, arguments: healerNotificationIndex);
            },
            leading: Icon(
              Icons.notifications,
              color: Theme.of(context).focusColor.withOpacity(1),
            ),
            title: Text(
              AppLocalizations.of(context)!.notifications,
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ),
          ListTile(
            onTap: () {
              currentUser.value.role?.name == AppConstants.ROLE_CLIENT
                  ? Navigator.of(context).pushNamed(pagesRoute, arguments: appointmentIndex)
                  : Navigator.of(context).pushNamed(healerPagesRoute, arguments: healerAppointmentIndex);
            },
            leading: Icon(
              Icons.calendar_today_rounded,
              color: Theme.of(context).focusColor.withOpacity(1),
            ),
            title: Text(
              AppLocalizations.of(context)!.my_appointments,
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ),
          /* ListTile(TODO: activate if you add Favorites products
            onTap: () {
              currentUser.value.role?.name == AppConstants.ROLE_CLIENT ?
              Navigator.of(context).pushNamed(pagesRoute, arguments: 4):  Navigator.of(context).pushNamed(healerPagesRoute, arguments: 4);
            },
            leading: Icon(
              Icons.favorite,
              color: Theme.of(context).focusColor.withOpacity(1),
            ),
            title: Text(
              AppLocalizations.of(context)!.favorite_products,
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ),*/
          ListTile(
            dense: true,
            title: Text(
              AppLocalizations.of(context)!.application_preferences,
              style: Theme.of(context).textTheme.bodyMedium,
            ),
            trailing: Icon(
              Icons.remove,
              color: Theme.of(context).focusColor.withOpacity(0.3),
            ),
          ),
          ListTile(
            onTap: () {
              if (currentUser.value.apiToken != null) {
                Navigator.of(context).pushNamed(settingsRoute);
              } else {
                Navigator.of(context).pushReplacementNamed(loginRoute);
              }
            },
            leading: Icon(
              Icons.settings,
              color: Theme.of(context).focusColor.withOpacity(1),
            ),
            title: Text(
              AppLocalizations.of(context)!.settings,
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ),
          /* ListTile(TODO: activate if you add local languages
            onTap: () {
              Navigator.of(context).pushNamed('/Languages');
            },
            leading: Icon(
              Icons.translate,
              color: Theme.of(context).focusColor.withOpacity(1),
            ),
            title: Text(
              S.of(context).languages,
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),*/
          ListTile(
            onTap: () {
              if (Theme.of(context).brightness == Brightness.dark) {
                setBrightness(Brightness.light);
                setting.value.brightness.value = Brightness.light;
              } else {
                setting.value.brightness.value = Brightness.dark;
                setBrightness(Brightness.dark);
              }
              setting.notifyListeners();
            },
            leading: Icon(
              Icons.brightness_6,
              color: Theme.of(context).focusColor.withOpacity(1),
            ),
            title: Text(
              Theme.of(context).brightness == Brightness.dark
                  ? AppLocalizations.of(context)!.light_mode
                  : AppLocalizations.of(context)!.dark_mode,
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ),
          currentUser.value.apiToken != null && currentUser.value.role?.name == AppConstants.ROLE_CLIENT
              ? ListTile(
                  onTap: () {
                    Navigator.of(context).pushReplacementNamed(healerRegisterRoute);
                  },
                  leading: Icon(
                    Icons.app_registration,
                    color: Theme.of(context).focusColor.withOpacity(1),
                  ),
                  title: Text(
                    AppLocalizations.of(context)!.register_as_healer,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                )
              : const SizedBox(height: 0),
          ListTile(
            onTap: () {
              if (currentUser.value.apiToken != null) {
                logout().then((value) {
                  Navigator.of(context)
                      .pushNamedAndRemoveUntil(pagesRoute, (Route<dynamic> route) => false, arguments: homeIndex);
                });
              } else {
                Navigator.of(context).pushNamed(loginRoute);
              }
            },
            leading: Icon(
              Icons.exit_to_app,
              color: Theme.of(context).focusColor.withOpacity(1),
            ),
            title: Text(
              currentUser.value.apiToken != null
                  ? AppLocalizations.of(context)!.log_out
                  : AppLocalizations.of(context)!.login,
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ),
          currentUser.value.apiToken == null
              ? ListTile(
                  onTap: () {
                    Navigator.of(context).pushNamed(signUpRoute);
                  },
                  leading: Icon(
                    Icons.person_add,
                    color: Theme.of(context).focusColor.withOpacity(1),
                  ),
                  title: Text(
                    AppLocalizations.of(context)!.register,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                )
              : const SizedBox(height: 0),
          setting.value.enableVersion
              ? ListTile(
                  dense: true,
                  title: Text(
                    "${AppLocalizations.of(context)!.version} ${setting.value.appVersion}",
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  trailing: Icon(
                    Icons.remove,
                    color: Theme.of(context).focusColor.withOpacity(0.3),
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  }
}
