import 'package:flutter/material.dart';

//import 'package:gogo_online/src/elements/CardsVertCarouselLoaderWidget.dart';
//import 'package:gogo_online/src/elements/CardsVertCarouselWidget.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../controllers/home_controller.dart';
//import '../elements/CardsCarouselWidget.dart';
//import '../elements/CategoriesCarouselWidget.dart';
import '../elements/CardsVertCarouselWidget.dart';
import '../elements/DeliveryAddressBottomSheetWidget.dart';
//import '../elements/GridWidget.dart';
//import '../elements/ProductsCarouselWidget.dart';
//import '../elements/ReviewsListWidget.dart';
import '../elements/SearchBarWidget.dart';
import '../elements/WaitingRoomButtonWidget.dart';
import '../repository/settings_repository.dart' as settings_repo;
import '../repository/user_repository.dart';


class HomeWidget extends StatefulWidget {
   final GlobalKey<ScaffoldState>? parentScaffoldKey;

 const HomeWidget({Key? key, this.parentScaffoldKey}) : super(key: key);
  @override
  State createState() => _HomeWidgetState();
}

class _HomeWidgetState extends StateMVC<HomeWidget> {
  late HomeController _con;

  _HomeWidgetState() : super(HomeController()) {
    _con = controller as HomeController;
  }

  @override
  void didUpdateWidget(covariant StatefulWidget oldWidget) {
    // TODO: implement didUpdateWidget
    _con.refreshHome();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey?.currentState?.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text("Gogo Online", style: Theme.of(context).textTheme.titleLarge!.merge(TextStyle(letterSpacing: 1.3)),),
        //TODO return this listener when you have setup the server
        /*ValueListenableBuilder(
          valueListenable: settingsRepo.setting,
          builder: (context, value, child) {
            return Text(
              value.appName ?? S.of(context).home,
              style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
            );
          },
        ),*/
        actions: <Widget>[
          //TODO re-evaluate the addition of this or change it to appointment
          //  new ShoppingCartButtonWidget(iconColor: Theme.of(context).hintColor, labelColor: Theme.of(context).accentColor),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _con.refreshHome,
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: SearchBarWidget(
                  onClickFilter: (event) {
                    widget.parentScaffoldKey?.currentState?.openEndDrawer();
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15, left: 20, right: 20),
                child: ListTile(
                  dense: true,
                  contentPadding: EdgeInsets.symmetric(vertical: 0),
                  leading: Icon(
                    Icons.circle,
                    color: Theme.of(context).hintColor,
                  ),
                  trailing: IconButton(
                    onPressed: () {
                      if (currentUser.value.apiToken == null) {
                        _con.requestForCurrentLocation(context);
                      } else {
                        var bottomSheetController = widget.parentScaffoldKey?.currentState?.showBottomSheet(
                              (context) => DeliveryAddressBottomSheetWidget(scaffoldKey: widget.parentScaffoldKey),
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                          ),
                        );
                        bottomSheetController?.closed.then((value) {
                          _con.refreshHome();
                        });
                      }
                    },
                    icon: Icon(
                      Icons.my_location,
                      color: Theme.of(context).hintColor,
                    ),
                  ),
                  title: Text(
                    AppLocalizations.of(context)!.healer_near_you,
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                  subtitle: Text(
                    "${AppLocalizations.of(context)!.near_to} ${settings_repo.billingAddress.value?.city ?? AppLocalizations.of(context)!.unknown}",
                    style: Theme.of(context).textTheme.bodySmall,
                  ),
                ),
              ),
              CardsVerticalCarouselWidget(healersList: _con.topHealers, heroTag: 'home_top_markets'),

            ],
          ),
        ),
      ),
    );
  }
}
