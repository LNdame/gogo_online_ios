import 'package:flutter/material.dart';
import 'package:gogo_online_ios/route_constants.dart';
import 'package:gogo_online_ios/src/helpers/app_constants.dart';
import 'package:gogo_online_ios/src/helpers/custom_trace.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
//import '../../generated/l10n.dart';
import '../repository/user_repository.dart' as user_repo;
import '../controllers/splash_screen_controller.dart';

class SplashScreen extends StatefulWidget {
  
  const SplashScreen({Key? key}): super(key:key);
  
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends StateMVC<SplashScreen> {
  late SplashScreenController con;

  SplashScreenState() : super(SplashScreenController()) {
    con = controller as SplashScreenController;
  }

  @override
  void initState() {
    super.initState();
   // loadData();
  }

  void loadData() {
    con.progress.addListener(() {
      double progress = 0;
      for (var prog in con.progress.value.values) {
        progress += prog;
      }
      if (progress >= 100) {
        try {
          if (user_repo.currentUser.value.apiToken == null) {
            Navigator.of(context).pushReplacementNamed(onboardingRoute);
          } else if (user_repo.currentUser.value.apiToken != null) {
            user_repo.currentUser.value.role?.name == AppConstants.ROLE_CLIENT
                ? Navigator.of(context).pushReplacementNamed('/Pages', arguments: 2)
                : Navigator.of(context).pushNamed('/HealerPages', arguments: 2);
          }
        } catch (e) {
          print(CustomTrace(StackTrace.current, message: e.toString()));
          Navigator.of(context).pushReplacementNamed(onboardingRoute);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size _size = MediaQuery.of(context).size;
    return Scaffold(
      key: con.scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
          color: AppConstants.customBackground,
        ),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/img/logo.png',
                width: 150,
                fit: BoxFit.cover,
              ),
              SizedBox(height: 50),
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).hintColor),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/*Container(
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
        ),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/img/logo.png',
                width: 150,
                fit: BoxFit.cover,
              ),
              SizedBox(height: 50),
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).hintColor),
              ),
            ],
          ),
        ),
      ),*/

/*Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(child: Container(
            child: OnboardingScreen(),
          )
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 26.0),
            child: Column(
              children: [
                Container(
                  width: _size.width,
                  height: AppConstants.buttonHeight,
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7.0),
                    ),
                    color: AppConstants.primaryColor,
                    onPressed: () {
                      Navigator.of(context).pushReplacementNamed('/SignUp');
                    },
                    child: Text(
                      S.of(context).get_started.toUpperCase(),
                      style: TextStyle(
                        color: AppConstants.whiteColor,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 12.0,
                ),
                Container(
                  width: _size.width,
                  height: AppConstants.buttonHeight,
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7.0),
                      side: BorderSide(
                        color: AppConstants.primaryColor,
                      ),
                    ),
                    color: AppConstants.whiteColor,
                    onPressed: () {
                      Navigator.of(context).pushReplacementNamed('/Login');
                    },
                    child: Text(
                      S.of(context).login.toUpperCase(),
                      style: TextStyle(
                        color: AppConstants.primaryColor,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                )
              ],
            ),
          )
        ],
      )*/
