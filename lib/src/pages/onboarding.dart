import 'package:flutter/material.dart';
import 'package:gogo_online_ios/src/controllers/onboarding_controller.dart';
import 'package:gogo_online_ios/src/elements/BlockButtonWidget.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../../route_constants.dart';
import '../helpers/app_constants.dart';
import '../widgets/onboarding_screen_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OnBoardingPage extends StatefulWidget {
  const OnBoardingPage({Key? key}) : super(key: key);

  @override
  State createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends StateMVC<OnBoardingPage> {

  late OnBoardingController con;
  late AppStateMVC appState;
  _OnBoardingPageState() : super(OnBoardingController()) {
    con = controller as OnBoardingController;
  }

  @override
  void initState() {
    super.initState();
    /// Retrieve the 'app level' State object
    appState = rootState!;
  }

  @override
  Widget build(BuildContext context) {
    final Size _size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/img/gogoonline.jpg"),
                fit: BoxFit.fill
            )
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
                child: Container(
              child: OnboardingScreenWidget(),
            )),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 26.0),
              child: Column(
                children: [
                  Container(
                    width: _size.width,
                    height: AppConstants.buttonHeight,
                    child: BlockButtonWidget(
                      text: Text(
                        AppLocalizations.of(context)!.get_started.toUpperCase(),
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      ),
                      color: Theme.of(context).colorScheme.secondary,
                      onPressed: () {
                        Navigator.of(context).pushReplacementNamed(signUpRoute);
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 12.0,
                  ),
                  Container(
                    width: _size.width,
                    height: AppConstants.buttonHeight,
                    child: BlockButtonWidget(
                      text: Text(
                        AppLocalizations.of(context)!.login.toUpperCase(),
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      ),
                      color: Theme.of(context).focusColor,
                      onPressed: () {
                        Navigator.of(context).pushReplacementNamed(loginRoute);
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
