import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:gogo_online_ios/route_constants.dart';
import 'package:gogo_online_ios/src/controllers/healer_controller.dart';
import 'package:gogo_online_ios/src/elements/BlockButtonWidget.dart';
import 'package:gogo_online_ios/src/elements/ProfileAvatarWidget.dart';
import 'package:gogo_online_ios/src/helpers/app_constants.dart';
import 'package:gogo_online_ios/src/helpers/helper.dart';
import 'package:gogo_online_ios/src/models/healer.dart';
import 'package:gogo_online_ios/src/repository/user_repository.dart';
import 'package:gogo_online_ios/src/utils/ValidatorUtil.dart';
import 'package:gogo_online_ios/src/widgets/time_picker_spinner.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';


class HealerRegistrationWidget extends StatefulWidget {
  @override
  _HealerRegistrationWidgetState createState() => _HealerRegistrationWidgetState();
}

class _HealerRegistrationWidgetState extends StateMVC<HealerRegistrationWidget> {
 late HealerController _con;
  bool _afrikaansChecked = false;
  bool _englishChecked = false;
  bool _sepediChecked = false;
  bool _sesothoChecked = false;
  bool _southernNdebeleChecked = false;
  bool _swaziChecked = false;
  bool _tsongaChecked = false;
  bool _tswanaChecked = false;
  bool _vendaChecked = false;
  bool _xhosaChecked = false;
  bool _zuluChecked = false;
  var _provinceList = <DropdownMenuItem<String>>[];
  var _languageList = <String>[];
  var _selectedProvinceValue;
  var _provinceName = <String>[];
 var _provinceCoord = <String>[];

 String selectedProvince = "";
 String writtenCity = "";
 String writtenStreetAddress = "";

  DateTime dateTimeStartWeek = DateTime.now();
  DateTime dateTimeEndWeek = DateTime.now();
  DateTime dateTimeStartSaturday = DateTime.now();
  DateTime dateTimeEndSaturday = DateTime.now();
  DateTime dateTimeStartSunday = DateTime.now();
  DateTime dateTimeSEndSunday = DateTime.now();


  void loadProvinces(){
    _provinceList.add(DropdownMenuItem(child: Text(AppConstants.EASTERN_CAPE), value: AppConstants.EASTERN_CAPE_LAT_LON,));
    _provinceList.add(DropdownMenuItem(child: Text(AppConstants.FREE_STATE), value: AppConstants.FREE_STATE_LAT_LON,));
    _provinceList.add(DropdownMenuItem(child: Text(AppConstants.GAUTENG), value: AppConstants.GAUTENG_LAT_LON,));
    _provinceList.add(DropdownMenuItem(child: Text(AppConstants.KWAZULU_NATAL), value: AppConstants.KWAZULU_NATAL_LAT_LON,));
    _provinceList.add(DropdownMenuItem(child: Text(AppConstants.LIMPOPO), value: AppConstants.LIMPOPO_LAT_LON,));
    _provinceList.add(DropdownMenuItem(child: Text(AppConstants.MPUMALANGA), value: AppConstants.MPUMALANGA_LAT_LON,));
    _provinceList.add(DropdownMenuItem(child: Text(AppConstants.NORTHERN_CAPE), value: AppConstants.NORTHERN_CAPE_LAT_LON,));
    _provinceList.add(DropdownMenuItem(child: Text(AppConstants.NORTH_WEST), value: AppConstants.NORTH_WEST_LAT_LON,));
    _provinceList.add(DropdownMenuItem(child: Text(AppConstants.WESTERN_CAPE), value: AppConstants.WESTERN_CAPE_LAT_LON,));

    _provinceName = [AppConstants.EASTERN_CAPE,AppConstants.FREE_STATE,AppConstants.GAUTENG,AppConstants.KWAZULU_NATAL,AppConstants.LIMPOPO,
      AppConstants.MPUMALANGA,AppConstants.NORTHERN_CAPE,AppConstants.NORTH_WEST,AppConstants.WESTERN_CAPE];
    _provinceCoord = [AppConstants.EASTERN_CAPE_LAT_LON,AppConstants.FREE_STATE_LAT_LON, AppConstants.GAUTENG_LAT_LON,
      AppConstants.KWAZULU_NATAL_LAT_LON, AppConstants.LIMPOPO_LAT_LON,AppConstants.MPUMALANGA_LAT_LON,
      AppConstants.NORTHERN_CAPE_LAT_LON,AppConstants.NORTH_WEST_LAT_LON,AppConstants.WESTERN_CAPE_LAT_LON];
  }

 late Healer requestHealer;
  _HealerRegistrationWidgetState(): super(HealerController()){
    _con = controller as HealerController;
  }

  @override
  void initState() {
    super.initState();
    initRequestHealer();
    loadProvinces();
  }

  void initRequestHealer(){
    requestHealer =  Healer.fromJSON({});
    requestHealer.name = currentUser.value.name??"";
    requestHealer.firebaseId = currentUser.value.firebaseUid??"";
    requestHealer.latitude ="-26.2708";
    requestHealer.longitude ="28.1123";
    requestHealer.closed = false;
    requestHealer.adminCommission = 10.0;
    requestHealer.defaultTax = 15;
    requestHealer.information = "Work hours \n";
    requestHealer.language ="";
  }

  void setLatLongFromProvince(String value){
    var valueSplit = value.split(',');
    if (valueSplit!=null){
       requestHealer.latitude = valueSplit[0] ?? "-26.2708";
       requestHealer.longitude = valueSplit[1] ?? "28.1123";
       print("${requestHealer.latitude} ${requestHealer.longitude}");
    }
  }

  void composeInformationMessage(){
    requestHealer.information = "Work hours \n ";
    requestHealer.information += "week days ${DateFormat('HH:mm').format(dateTimeStartWeek)} - ${DateFormat('HH:mm').format(dateTimeEndWeek)} "
        "Saturday working hours ${DateFormat('HH:mm').format(dateTimeStartSaturday)} - ${DateFormat('HH:mm').format(dateTimeEndSaturday)} "
        "Sunday working hours ${DateFormat('HH:mm').format(dateTimeStartSunday)} - ${DateFormat('HH:mm').format(dateTimeSEndSunday)} ";
    if (kDebugMode) {
      print(requestHealer.information);
    }
  }

 void composeAddress() {
    var address = "$writtenStreetAddress,* $writtenCity #, $selectedProvince";
    requestHealer.address = address;
  }

  void addElementToLanguageList(String lang) {
    _languageList.add(lang);
  }

  void removeElementToLanguageList(String lang) {
    if (_languageList.contains(lang)) _languageList.remove(lang);
  }

  void addLanguagesList(){
    String langs = "";
    langs += _languageList.join(", ");
    requestHealer.language = langs;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _con.scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pushReplacementNamed('/Pages', arguments: homeIndex);
            },
            icon: const Icon(Icons.arrow_back),
            color: Theme.of(context).hintColor,
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: Text(
            AppLocalizations.of(context)!.register_as_healer,
            style: Theme.of(context).textTheme.titleLarge!.merge(const TextStyle(letterSpacing: 1.3)),
          ),
        ),
        body: SingleChildScrollView(
          padding:const EdgeInsets.symmetric(horizontal: 20, vertical: 10),

          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              ProfileAvatarWidget(user: currentUser.value),
              const SizedBox(height: 30,),
              Form(key: _con.registerFormKey,
                  autovalidateMode: AutovalidateMode.disabled,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextFormField(
                        keyboardType: TextInputType.text,
                        validator: (input)=>ValidatorUtil.genericEmptyValidator(input!, "Please enter a name"),
                        onSaved: (input) =>requestHealer.name = input!,
                        decoration: InputDecoration(
                          labelText: "Practice Name",
                          labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                          contentPadding: const EdgeInsets.all(12),
                          hintText: AppLocalizations.of(context)!.siya_nkosi,
                          hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.7)),
                          prefixIcon: Icon(Icons.album_rounded, color: Theme.of(context).colorScheme.secondary),
                          border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.5))),
                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                        ),
                      ),
                      const SizedBox(height: 30,),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        validator: (input)=>ValidatorUtil.genericEmptyValidator(input!, "Please enter a street address"),
                        onSaved: (input) =>writtenStreetAddress = input!,
                        decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)!.street_address,
                          labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                          contentPadding:const EdgeInsets.all(12),
                          hintText: AppLocalizations.of(context)!.siya_nkosi,
                          hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.7)),
                          prefixIcon: Icon(Icons.location_on_outlined, color: Theme.of(context).colorScheme.secondary),
                          border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.5))),
                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                        ),
                      ),
                      const SizedBox(height: 30,),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        validator: (input)=>ValidatorUtil.genericEmptyValidator(input!, "Please enter a city"),
                        onSaved: (input) => writtenCity = input!,
                        decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)!.city,
                          labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                          contentPadding:const EdgeInsets.all(12),
                          hintText: AppLocalizations.of(context)!.siya_nkosi,
                          hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.7)),
                          prefixIcon: Icon(Icons.location_on_outlined, color: Theme.of(context).colorScheme.secondary),
                          border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.5))),
                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                        ),
                      ),
                      const SizedBox(height: 30,),
                      DropdownButtonFormField<String>(
                        value: _selectedProvinceValue,
                        items: _provinceList,
                        validator: (value) => value==null?'field required':null,
                        onChanged: (value) {
                          setLatLongFromProvince(value as String);
                         var index = _provinceCoord.indexOf(value);
                         setState(() {
                           selectedProvince = _provinceName[index];
                         });

                        },
                        decoration: InputDecoration(
                          labelText:"Province",  //S.of(context).full_address,
                          labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                          contentPadding: const EdgeInsets.all(12),
                          hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.7)),
                          prefixIcon: Icon(Icons.location_on_outlined, color: Theme.of(context).colorScheme.secondary),
                          border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.5))),
                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                        ),
                      ),

                      const SizedBox(height: 30,),
                      Text("Consultation language", style: Theme.of(context).textTheme.headlineMedium,),
                      const SizedBox(height: 16,),
                      CheckboxListTile(value: _afrikaansChecked,
                        onChanged: (value){
                          setState(() {_afrikaansChecked =value!;
                          value == true ? addElementToLanguageList("Afrikaans" ):removeElementToLanguageList("Afrikaans");});
                        },
                        title: Text("Afrikaans", style: Theme.of(context).textTheme.bodySmall),
                        selectedTileColor: Theme.of(context).colorScheme.secondary,
                        controlAffinity: ListTileControlAffinity.leading,),
                      const SizedBox(height: 12,),
                      CheckboxListTile(value: _englishChecked,
                        onChanged: (value){
                          setState(() {_englishChecked =value!;
                          value == true ? addElementToLanguageList("English" ):removeElementToLanguageList("English");
                          });
                        },
                        title: Text("English", style: Theme.of(context).textTheme.bodySmall),
                        selectedTileColor: Theme.of(context).colorScheme.secondary,
                        controlAffinity: ListTileControlAffinity.leading,),
                      const SizedBox(height: 12,),

                      CheckboxListTile(value: _sepediChecked,
                        onChanged: (value){
                          setState(() {_sepediChecked =value!;
                          value == true ? addElementToLanguageList("Sepedi" ):removeElementToLanguageList("Sepedi");});
                        },
                        title: Text("Sepedi", style: Theme.of(context).textTheme.bodySmall),
                        selectedTileColor: Theme.of(context).colorScheme.secondary,
                        controlAffinity: ListTileControlAffinity.leading,),
                      const SizedBox(height: 12,),
                      CheckboxListTile(value: _sesothoChecked,
                        onChanged: (value){
                          setState(() {_sesothoChecked =value!;
                          value == true ? addElementToLanguageList("Sesotho" ):removeElementToLanguageList("Sesotho");});
                        },
                        title: Text("Sesotho", style: Theme.of(context).textTheme.bodySmall),
                        selectedTileColor: Theme.of(context).colorScheme.secondary,
                        controlAffinity: ListTileControlAffinity.leading,),
                      const SizedBox(height: 12,),
                      CheckboxListTile(value: _southernNdebeleChecked,
                        onChanged: (value){
                          setState(() {_southernNdebeleChecked =value!;
                          value == true ? addElementToLanguageList("isiNdebele" ):removeElementToLanguageList("isiNdebele");});
                        },
                        title: Text("isiNdebele", style: Theme.of(context).textTheme.bodySmall),
                        selectedTileColor: Theme.of(context).colorScheme.secondary,
                        controlAffinity: ListTileControlAffinity.leading,),
                      const SizedBox(height: 12,),
                      CheckboxListTile(value: _swaziChecked,
                        onChanged: (value){
                          setState(() {_swaziChecked =value!;
                          value == true ? addElementToLanguageList("siSwati" ):removeElementToLanguageList("siSwati");});
                        },
                        title: Text("siSwati", style: Theme.of(context).textTheme.bodySmall),
                        selectedTileColor: Theme.of(context).colorScheme.secondary,
                        controlAffinity: ListTileControlAffinity.leading,),
                      const SizedBox(height: 12,),
                      CheckboxListTile(value: _tsongaChecked,
                        onChanged: (value){
                          setState(() {_tsongaChecked =value!;
                          value == true ? addElementToLanguageList("Xitsonga" ):removeElementToLanguageList("Xitsonga");});
                        },
                        title: Text("Xitsonga", style: Theme.of(context).textTheme.bodySmall),
                        selectedTileColor: Theme.of(context).colorScheme.secondary,
                        controlAffinity: ListTileControlAffinity.leading,),
                      const SizedBox(height: 12,),
                      CheckboxListTile(value: _tswanaChecked,
                        onChanged: (value){
                          setState(() {_tswanaChecked =value!;
                          value == true ? addElementToLanguageList("Setswana" ):removeElementToLanguageList("Setswana");});
                        },
                        title: Text("Setswana", style: Theme.of(context).textTheme.bodySmall),
                        selectedTileColor: Theme.of(context).colorScheme.secondary,
                        controlAffinity: ListTileControlAffinity.leading,),
                      const SizedBox(height: 12,),
                      CheckboxListTile(value: _vendaChecked,
                        onChanged: (value){
                          setState(() {_vendaChecked =value!;
                          value == true ? addElementToLanguageList("Tshivenda" ):removeElementToLanguageList("Tshivenda");});
                        },
                        title: Text("Tshivenda", style: Theme.of(context).textTheme.bodySmall),
                        selectedTileColor: Theme.of(context).colorScheme.secondary,
                        controlAffinity: ListTileControlAffinity.leading,),
                      const SizedBox(height: 12,),
                      CheckboxListTile(value: _xhosaChecked,
                        onChanged: (value){
                          setState(() {_xhosaChecked =value!;
                          value == true ? addElementToLanguageList("isiXhosa" ):removeElementToLanguageList("isiXhosa");});
                        },
                        title: Text("isiXhosa", style: Theme.of(context).textTheme.bodySmall),
                        selectedTileColor: Theme.of(context).colorScheme.secondary,
                        controlAffinity: ListTileControlAffinity.leading,),
                      const SizedBox(height: 12,),
                      CheckboxListTile(value: _zuluChecked,
                        onChanged: (value){
                          setState(() {_zuluChecked =value!;
                          value == true ? addElementToLanguageList("isiZulu" ):removeElementToLanguageList("isiZulu");
                          });
                        },
                        title: Text("isiZulu", style: Theme.of(context).textTheme.bodySmall),
                        //   selectedTileColor: Theme.of(context).colorScheme.secondary,
                        controlAffinity: ListTileControlAffinity.leading,),
                      const SizedBox(height: 12,),

                      TextFormField(
                        keyboardType: TextInputType.text,
                        maxLines: 5,
                        onSaved: (input) =>requestHealer.description = input!,
                        decoration: InputDecoration(
                          labelText: "Description of services",//S.of(context).full_address,
                          labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                          contentPadding:const EdgeInsets.all(12),
                          hintText: AppLocalizations.of(context)!.siya_nkosi,
                          hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.7)),
                          prefixIcon: Icon(Icons.notes_outlined, color: Theme.of(context).colorScheme.secondary),
                          border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.5))),
                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                        ),
                      ),
                      const SizedBox(height: 30,),
                      ////week day hours
                      Text("Week days working hours", style: Theme.of(context).textTheme.titleLarge,),
                      SizedBox(height: 130,
                        child:Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text("Open"),
                            PickerHourMinute24h(timeChanged: (time){ dateTimeStartWeek = time;}),
                            Text("Closing"),
                            PickerHourMinute24h(timeChanged: (time){ dateTimeEndWeek = time;}),
                          ],
                        ),
                      ),
                      ////Saturday hours
                      Text("Saturday working hours", style: Theme.of(context).textTheme.titleLarge,),
                      SizedBox(height: 130,
                        child:Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text("Open"),
                            PickerHourMinute24h(timeChanged: (time){ dateTimeStartSaturday = time;}),
                            Text("Closing"),
                            PickerHourMinute24h(timeChanged: (time){ dateTimeEndSaturday = time;}),
                          ],
                        ),
                      ),
                      ////Sunday hours
                      Text("Sunday working hours", style: Theme.of(context).textTheme.titleLarge,),
                      SizedBox(height: 130,
                        child:Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text("Open"),
                            PickerHourMinute24h(timeChanged: (time){ dateTimeStartSunday = time;}),
                            Text("Closing"),
                            PickerHourMinute24h(timeChanged: (time){ dateTimeSEndSunday = time;}),
                          ],
                        ),
                      ),

                      const SizedBox(height: 30,),
                      TextFormField(
                        keyboardType: TextInputType.number,
                        validator: (input)=>ValidatorUtil.genericEmptyValidator(input!, "Please enter an amount"),
                        onSaved: (input) =>requestHealer.hourlyPrice = double.parse(input!),
                        decoration: InputDecoration(
                          labelText: "Consultation price (per hour)",
                          labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                          contentPadding: const EdgeInsets.all(12),
                          hintText:"500",//
                          hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.7)),
                          prefixIcon: Icon(Icons.money, color: Theme.of(context).colorScheme.secondary),
                          border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.5))),
                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                        ),
                      ),
                      const SizedBox(height: 30,),
                      TextFormField(
                        keyboardType: TextInputType.text,

                        onSaved: (input) =>requestHealer.practiceNumber  =input!,
                        decoration: InputDecoration(
                          labelText: "Practice number",
                          labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                          contentPadding:const EdgeInsets.all(12),
                          hintText:"MP 075837583",
                          hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.7)),
                          prefixIcon: Icon(Icons.wb_shade, color: Theme.of(context).colorScheme.secondary),
                          border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.5))),
                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                        ),
                      ),
                      const SizedBox(height: 30,),
                      TextFormField(
                        keyboardType: TextInputType.phone,
                        validator: ValidatorUtil.phoneValidator,
                        onSaved: (input) =>requestHealer.mobile =input!,
                        decoration: InputDecoration(
                          labelText: "Practice Contact Number (optional)",
                          labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
                          contentPadding:const EdgeInsets.all(12),
                          hintText:"012 566 7685",
                          hintStyle: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.7)),
                          prefixIcon: Icon(Icons.phone, color: Theme.of(context).colorScheme.secondary),
                          border: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.5))),
                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                        ),
                      ),
                      const SizedBox(height: 30,),
                      BlockButtonWidget(
                        text: Text(
                          AppLocalizations.of(context)!.register,
                          style: TextStyle(color: Theme.of(context).primaryColor),
                        ),
                        color: Theme.of(context).colorScheme.secondary,
                        onPressed: () {
                          if (_languageList.isNotEmpty) {
                            addLanguagesList();
                            composeAddress();
                            composeInformationMessage();
                            _con.requestRegisterHealer(requestHealer);
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                              content: Text("Please select a language"),
                            ));
                          }
                        },
                      ),
                    ],
                  )
              )
            ],
          ),

        )
    );
  }
}


class PickerHourMinute24h extends StatelessWidget {
  const PickerHourMinute24h({Key? key, required this.timeChanged}) : super(key: key);
  final ValueChanged<DateTime> timeChanged;

  @override
  Widget build(BuildContext context) {
    return TimePickerSpinner(
      is24HourMode: true,
      itemHeight: 30,
      isForce2Digits: true,
      minutesInterval: 15,
      normalTextStyle: TextStyle(fontSize: 24, color: Colors.black54),
      highlightedTextStyle: TextStyle(fontSize: 26, color: Colors.black),
      onTimeChange: timeChanged,
    );
  }
}

