import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:gogo_online_ios/src/controllers/consultation_controller.dart';
import 'package:gogo_online_ios/src/elements/ConsultationItemWithDateWidget.dart';
import 'package:gogo_online_ios/src/elements/EmptyConsultationsWidget.dart';
import 'package:gogo_online_ios/src/elements/PermissionDeniedWidget.dart';
import 'package:gogo_online_ios/src/elements/SearchBarWidget.dart';
import 'package:gogo_online_ios/src/elements/WaitingRoomButtonWidget.dart';
import 'package:gogo_online_ios/src/repository/user_repository.dart';
import 'package:mvc_pattern/mvc_pattern.dart';


class HealerConsultationsWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState>? parentScaffoldKey;

  const HealerConsultationsWidget({Key? key, this.parentScaffoldKey}) : super(key: key);

  @override
 State createState() => _HealerConsultationsWidgetState();
}

class _HealerConsultationsWidgetState extends StateMVC<HealerConsultationsWidget> {
  late ConsultationController _con;

  _HealerConsultationsWidgetState() : super(ConsultationController()) {
    _con = controller as ConsultationController;
  }

  @override
  void initState() {
    super.initState();
    _con.listenForHealerConsultations(message: 'Loaded');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        leading:  IconButton(
          icon:  Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey?.currentState?.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context)!.my_consultations,
          style: Theme.of(context).textTheme.headline6!.merge(const TextStyle(letterSpacing: 1.3)),
        ),

      ),
      body: currentUser.value.apiToken == null
          ? const PermissionDeniedWidget()
          :  _con.consultations.isEmpty
          ? EmptyConsultationsWidget()
          : RefreshIndicator(
        onRefresh: _con.refreshConsultations,
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: SearchBarWidget(),
                ),
                SizedBox(height: 20),
                ListView.separated(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  primary: false,
                  itemBuilder: (context, index) {
                    var consultation = _con.consultations.elementAt(index);
                    return ConsultationItemWithDateWidget(
                      expanded: true,
                      consultation: consultation,
                      onCanceled: (e) {
                        _con.doCancelConsultation(consultation);
                      },
                    );
                  },
                  separatorBuilder: (context, index) {
                    return SizedBox(height: 2);
                  },
                  itemCount: _con.consultations.length,
                ),
              ]),
        ),
      ),
    );
  }
}

