import 'package:flutter/material.dart';
import 'package:gogo_online_ios/route_constants.dart';
import 'package:gogo_online_ios/src/elements/DrawerWidget.dart';
import 'package:gogo_online_ios/src/elements/FilterWidget.dart';
import 'package:gogo_online_ios/src/helpers/helper.dart';
import 'package:gogo_online_ios/src/pages/healer/healer_consultations.dart';
import 'package:gogo_online_ios/src/pages/messaging/active_chat_screen.dart';
import '../../models/route_argument.dart';
import '../home.dart';
import '../favorites.dart';
//import '../map.dart';
import '../notifications.dart';
//import '../orders.dart';


// ignore: must_be_immutable
class HealerPagesWidget extends StatefulWidget {
  late dynamic currentTab;
 late RouteArgument routeArgument;
  Widget currentPage = HomeWidget();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  HealerPagesWidget({ Key? key,
    this.currentTab,}){
    if (currentTab != null) {
      if (currentTab is RouteArgument) {
        routeArgument = currentTab;
        currentTab = int.parse(currentTab.id);
      }
    } else {
      currentTab = homeIndex;
    }
  }

  @override
  _HealerPagesWidgetState createState() => _HealerPagesWidgetState();
}

class _HealerPagesWidgetState extends State<HealerPagesWidget> {

  initState() {
    super.initState();
    _selectTab(widget.currentTab);
  }

  @override
  void didUpdateWidget(HealerPagesWidget oldWidget) {
    _selectTab(oldWidget.currentTab);
    super.didUpdateWidget(oldWidget);
  }

  void _selectTab(int tabItem) {
    setState(() {
      widget.currentTab = tabItem;
      switch (tabItem) {
        case 0:
          widget.currentPage = HealerConsultationsWidget(parentScaffoldKey: widget.scaffoldKey);
          break;
        case 1:
          widget.currentPage = HomeWidget(parentScaffoldKey: widget.scaffoldKey);
          break;
        case 2:
          widget.currentPage = NotificationsWidget(parentScaffoldKey: widget.scaffoldKey);
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        key: widget.scaffoldKey,
        drawer: DrawerWidget(),
        endDrawer: FilterWidget(onFilter: (filter) {
          Navigator.of(context).pushReplacementNamed('/HealerPages', arguments: widget.currentTab);
        }),
        body: widget.currentPage,
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Theme.of(context).colorScheme.secondary,
          selectedFontSize: 0,
          unselectedFontSize: 0,
          iconSize: 22,
          elevation: 0,
          backgroundColor: Colors.transparent,
          selectedIconTheme: IconThemeData(size: 28),
          unselectedItemColor: Theme.of(context).focusColor.withOpacity(1),
          currentIndex: widget.currentTab,
          onTap: (int i) {
            this._selectTab(i);
          },
          // this will be set when a new tab is tapped
          items: [
            const BottomNavigationBarItem(
              icon: Icon(Icons.calendar_today),
              label: "",
            ),
           /* const BottomNavigationBarItem(
              icon: Icon(Icons.calendar_today),
              label: "",
            ),*/
            BottomNavigationBarItem(
                label: "",
                icon: Container(
                  width: 42,
                  height: 42,
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.secondary,
                    borderRadius: BorderRadius.all(
                      Radius.circular(50),
                    ),
                    boxShadow: [
                      BoxShadow(color: Theme.of(context).colorScheme.secondary.withOpacity(0.4), blurRadius: 40, offset: Offset(0, 15)),
                      BoxShadow(color: Theme.of(context).colorScheme.secondary.withOpacity(0.4), blurRadius: 13, offset: Offset(0, 3))
                    ],
                  ),
                  child:  Icon(Icons.home, color: Theme.of(context).primaryColor),
                )),
            const BottomNavigationBarItem(
              icon:  Icon(Icons.notifications),
              label: "",
            ),
            /*const BottomNavigationBarItem(
              icon:  Icon(Icons.settings),
              label: "",
            ),*/
          ],
        ),
      ),
    );
  }
}
