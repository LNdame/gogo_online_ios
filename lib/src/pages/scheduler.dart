import 'package:flutter/material.dart';
import 'package:gogo_online_ios/src/controllers/cart_controller.dart';
import 'package:gogo_online_ios/src/elements/AppointmentSlotWidget.dart';
import 'package:gogo_online_ios/src/elements/BlockButtonWidget.dart';
import 'package:gogo_online_ios/src/helpers/helper.dart';
import 'package:gogo_online_ios/src/utils/ValidatorUtil.dart';
import 'package:table_calendar/table_calendar.dart';
import '../models/cart.dart';
import '../models/route_argument.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SchedulerWidget extends StatefulWidget {
  final RouteArgument? routeArgument;
  final Cart cart;
  final CartController _con;

  const SchedulerWidget({Key? key, required this.cart, this.routeArgument, required CartController con})
      : _con = con,
        super(key: key);

  @override
  State createState() => _SchedulerWidgetState();
}

class _SchedulerWidgetState extends State<SchedulerWidget> {
  late Map<DateTime, List<dynamic>> _events;
  String selectedTime = "";
  late List<dynamic> _selectedEvents;
  late DateTime _selectedDate;
  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  late DateTime _selectedDay;
  late DateTime kNow;
  late DateTime kFirstDay;
  late DateTime kLastDay;

  @override
  void initState() {
    super.initState();
    _selectedDay = DateTime.now();
    _events = {};
    _selectedEvents = [];
    _selectedDate = new DateTime.now();

    kNow = DateTime.now();
    kFirstDay = DateTime(kNow.year, kNow.month - 3, kNow.day);
    kLastDay = DateTime(kNow.year, kNow.month + 3, kNow.day);
  }

  Map<String, dynamic> encodeMap(Map<DateTime, dynamic> map) {
    Map<String, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[key.toString()] = map[key];
    });
    return newMap;
  }

  Map<DateTime, dynamic> decodeMap(Map<String, dynamic> map) {
    Map<DateTime, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[DateTime.parse(key)] = map[key];
    });
    return newMap;
  }

  Future<void> saveConsultationDateTime(String selectedTime, DateTime date) async {
    widget.cart.consultationStartTime = selectedTime;
    String selectedDate =
        "${date.year}-${ValidatorUtil.convertDateToZeroFormat(date.month)}-${ValidatorUtil.convertDateToZeroFormat(date.day)}";
    widget.cart.consultationDate = selectedDate;
    await widget._con.updateDateTime(widget.cart);
  }

  @override
  Widget build(BuildContext context) {
    final setSelectedTime = (String val) {
      setState(() {
        selectedTime = val;
      });
    };

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          onPressed: () {
            if (widget.routeArgument != null) {
              Navigator.of(context).pushReplacementNamed(widget.routeArgument?.param,
                  arguments: RouteArgument(id: widget.routeArgument?.id));
            } else {
              Navigator.of(context).pushNamed('/Cart', arguments: RouteArgument(param: '/Pages', id: '2'));
            }
          },
          icon: const Icon(Icons.arrow_back),
          color: Theme.of(context).hintColor,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context)!.scheduler,
          style: Theme.of(context).textTheme.headline6!.merge(TextStyle(letterSpacing: 1.3)),
        ),
      ),
      bottomNavigationBar: Container(
          height: 100,
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 26),
          child: BlockButtonWidget(
            text: Text(
              AppLocalizations.of(context)!.set_consultation_date,
              style: TextStyle(color: Theme.of(context).primaryColor),
            ),
            color: Theme.of(context).colorScheme.secondary,
            onPressed: () async {
              await saveConsultationDateTime(selectedTime, _selectedDay).then((value) {
                Navigator.of(context).pushNamed('/Cart', arguments: RouteArgument(param: '/Pages', id: '2'));
              }, onError: (e) => print(e));
            },
          )),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding:const EdgeInsets.only(top: 15, left: 20, right: 20),
              child: ListTile(
                dense: true,
                contentPadding:const EdgeInsets.symmetric(vertical: 0),
                leading: Icon(
                  Icons.calendar_today_rounded,
                  color: Theme.of(context).hintColor,
                ),
                title: Text(
                  widget.cart.product.name,
                  style: Theme.of(context).textTheme.headline2,
                ),
                subtitle: Helper.getPrice(widget.cart.product.price, context,
                    style: Theme.of(context).textTheme.bodyText2, zeroPlaceholder: 'Free'),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            TableCalendar(
              firstDay: kFirstDay,
              lastDay: kLastDay,
              focusedDay: _focusedDay,
              calendarFormat: _calendarFormat,
              selectedDayPredicate: (day) {
                // Use `selectedDayPredicate` to determine which day is currently selected.
                // If this returns true, then `day` will be marked as selected.

                // Using `isSameDay` is recommended to disregard
                // the time-part of compared DateTime objects.
                return isSameDay(_selectedDay, day);
              },
              onDaySelected: (selectedDay, focusedDay) {
                if (!isSameDay(_selectedDay, selectedDay)) {
                  // Call `setState()` when updating the selected day
                  setState(() {
                    _selectedDay = selectedDay;
                    _focusedDay = focusedDay;
                  });
                }
              },
              onFormatChanged: (format) {
                if (_calendarFormat != format) {
                  // Call `setState()` when updating calendar format
                  setState(() {
                    _calendarFormat = format;
                  });
                }
              },
              onPageChanged: (focusedDay) {
                // No need to call `setState()` here
                _focusedDay = focusedDay;
              },
            ),
            SizedBox(
              height: 20.0,
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: AppointmentSlotWidget(
                timeOfDay: "Appointment Slot",
                times: [
                  "07:00",
                  "08:00",
                  "09:00",
                  "10:00",
                  "11:00",
                  "12:00",
                  "13:00",
                  "14:00",
                  "15:00",
                  "16:00",
                  "17:00",
                  "18:00",
                  "19:00",
                  "20:00",
                  "21:00"
                ],
                setSelectedTime: setSelectedTime,
                selectedTime: selectedTime,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
