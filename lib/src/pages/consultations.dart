import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../controllers/consultation_controller.dart';
import '../elements/ConsultationItemWithDateWidget.dart';
import '../elements/SearchBarWidget.dart';
import '../repository/user_repository.dart';
import '../elements/EmptyConsultationsWidget.dart';
import '../elements/PermissionDeniedWidget.dart';
import '../elements/WaitingRoomButtonWidget.dart';

class ConsultationsWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState>? parentScaffoldKey;

  const ConsultationsWidget({Key? key, this.parentScaffoldKey}) : super(key: key);

  @override
  State createState() => _ConsultationsWidgetState();
}

class _ConsultationsWidgetState extends StateMVC<ConsultationsWidget> {
  late ConsultationController _con;

  _ConsultationsWidgetState() : super(ConsultationController()) {
    _con = controller as ConsultationController;
  }

  @override
  void initState() {
    super.initState();
    _con.listenForConsultations();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        leading:  IconButton(
          icon:  Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey?.currentState?.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context)!.my_consultations,
          style: Theme.of(context).textTheme.headline6!.merge(const TextStyle(letterSpacing: 1.3)),
        ),
        actions: <Widget>[
           WaitingRoomButtonWidget(
              iconColor: Theme.of(context).hintColor, labelColor: Theme.of(context).colorScheme.secondary),
        ],
      ),
      body: currentUser.value.apiToken == null
          ? const PermissionDeniedWidget()
          :  _con.consultations.isEmpty
          ? EmptyConsultationsWidget()
          : RefreshIndicator(
        onRefresh: _con.refreshConsultations,
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: SearchBarWidget(),
                ),
                SizedBox(height: 20),
                ListView.separated(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  primary: false,
                  itemBuilder: (context, index) {
                    var consultation = _con.consultations.elementAt(index);
                    return ConsultationItemWithDateWidget(
                      expanded: true,
                      consultation: consultation,
                      onCanceled: (e) {
                        _con.doCancelConsultation(consultation);
                      },
                    );
                  },
                  separatorBuilder: (context, index) {
                    return SizedBox(height: 2);
                  },
                  itemCount: _con.consultations.length,
                ),
              ]),
        ),
      ),
    );
  }
}
