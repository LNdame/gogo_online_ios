import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:gogo_online_ios/src/utils/ThemeUtil.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart' as loc;
import '../repository/user_repository.dart' as user_repo;
import 'package:geolocator/geolocator.dart' ;
import 'package:geocoding/geocoding.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../helpers/custom_trace.dart';
import '../models/user.dart';
import '../models/address.dart';
import '../models/coupon.dart';
import '../models/setting.dart';

ValueNotifier<Setting> setting =  ValueNotifier( Setting());
ValueNotifier<Address> billingAddress =  ValueNotifier( Address.fromJSON({}));
Coupon coupon =  Coupon.fromJSON({});
final navigatorKey = GlobalKey<NavigatorState>();
//LocationData locationData;

Future<Setting> initSettings() async {
  Setting _setting;

  try {
    var themeData = ThemeUtil.themeToMap();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('settings', json.encode(themeData));
    _setting = Setting.fromJSON(themeData);
    if (prefs.containsKey('language')) {
      _setting.mobileLanguage.value = Locale(prefs.getString('language')??"", '');
    }
    _setting.brightness.value = prefs.getBool('isDark') ?? false ? Brightness.dark : Brightness.light;
    setting.value = _setting;
    // ignore: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member
    setting.notifyListeners();

  } catch (e) {
    print(CustomTrace(StackTrace.current, message: "no settings found").toString());
    return Setting.fromJSON({});
  }
  return setting.value;
}

Future<dynamic> setCurrentLocation() async {
  var location = loc.Location();
  User _user = user_repo.currentUser.value;
  final Position position = await _determinePosition();

  final List<Placemark> placemarks = await placemarkFromCoordinates(position.latitude, position.longitude);
  Address currentAddress = Address.fromJSON({});

  if(placemarks!=null && placemarks.isNotEmpty){
    final Placemark placemark = placemarks.first;
    final String? city = placemark.locality;
    print("City : $city");

    var streetAddress ="${placemark.street??""}, ${placemark.subLocality??""}, ${placemark.locality??""},"
        " ${placemark.administrativeArea??""}, ${placemark.postalCode??""}" ;
    print("Name : $streetAddress");
    currentAddress = Address.fromJSON({'description':placemark.locality??"",
      'address': streetAddress, 'latitude': position.latitude, 'longitude': position.longitude,
      'user_id':_user.id
    });
    changeCurrentLocation(currentAddress);
  }

  return currentAddress;

  /*var location =  Location();
  MapsUtil mapsUtil =  MapsUtil();
  final whenDone =  Completer();
  Address _address = Address.fromJSON({});
  location.requestService().then((value) async {
    location.getLocation().then((locationData) async {
      String addressName = await mapsUtil.getAddressName(LatLng(locationData.latitude??0, locationData.longitude??0), setting.value.googleMapsKey)??"";
      _address = Address.fromJSON({'address': addressName, 'latitude': locationData.latitude, 'longitude': locationData.longitude});
      await changeCurrentLocation(_address);
      whenDone.complete(_address);
    }).timeout(Duration(seconds: 10), onTimeout: () async {
      await changeCurrentLocation(_address);
      whenDone.complete(_address);
      return null;
    }).catchError((e) {
      whenDone.complete(_address);
    });
  });
  return whenDone.future;*/
}

Future<Position> _determinePosition() async{
  
  bool serviceEnabled;
  LocationPermission permission;
  
  serviceEnabled =await Geolocator.isLocationServiceEnabled();
  if(!serviceEnabled){
    return Future.error('Location services are disabled.');
  }
  
  permission = await Geolocator.checkPermission();
  if(permission==LocationPermission.denied){
    permission = await Geolocator.requestPermission();
    if(permission ==LocationPermission.denied){
      return Future.error('Location permission are denied.');
    }
  }
  
  if(permission == LocationPermission.deniedForever){
    return Future.error('Location permissions are permanently denied, we cannot request permissions.');
  }
  return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.medium);
}



Future<Address> changeCurrentLocation(Address _address) async {
  if (!_address.isUnknown()) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('delivery_address', json.encode(_address.toMap()));
  }
  return _address;
}

Future<Address> getCurrentLocation() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  //await prefs.clear();
  try {
    if (prefs.containsKey('delivery_address')) {
      billingAddress.value = Address.fromJSON(json.decode(prefs.getString('delivery_address') ?? ""));
      return billingAddress.value;
    } else {
      billingAddress.value = Address.fromJSON({});
      return Address.fromJSON({});
    }
  } catch (e) {
    // Do Nothing
    return Address.fromJSON({});
  }
}

void setBrightness(Brightness brightness) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (brightness == Brightness.dark) {
    prefs.setBool("isDark", true);
    brightness = Brightness.dark;
  } else {
    prefs.setBool("isDark", false);
    brightness = Brightness.light;
  }
}

Future<void> setDefaultLanguage(String? language) async {
  if (language != null) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('language', language);
  }
}

Future<String> getDefaultLanguage(String defaultLanguage) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey('language')) {
    defaultLanguage = prefs.getString('language')??"";
  }
  return defaultLanguage;
}

Future<void> saveMessageId(String? messageId) async {
  if (messageId != null) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('google.message_id', messageId);
  }
}

Future<String> getMessageId() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString('google.message_id')??"";
}
