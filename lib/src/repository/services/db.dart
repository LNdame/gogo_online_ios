
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';

import '../../helpers/app_constants.dart';
import '../../helpers/custom_trace.dart';
import '../../models/media_model.dart';
import '../../models/message.dart';

class DB{

  final CollectionReference _usersCollection =
  FirebaseFirestore.instance.collection(AppConstants.USERS_COLLECTION);
  final CollectionReference _messagesCollection =
  FirebaseFirestore.instance.collection(AppConstants.ALL_MESSAGES_COLLECTION);

  Future<QuerySnapshot<Map<String, dynamic>>> getNewChats(String groupChatId, DocumentSnapshot lastSnapshot, [int limit = 20]) {
    try {
      return _messagesCollection
          .doc(groupChatId)
          .collection(AppConstants.CHATS_COLLECTION)
          .startAfterDocument(lastSnapshot)
          .limit(20)
          .orderBy('timeStamp', descending: true)
          .get();
    } catch (error) {
      print('****************** DB getSnapshotsAfter error **********************');
      print(error);
      throw error;
    }
  }

  void addNewMessage(String groupId, DateTime timeStamp, dynamic data) {
    try {
      _messagesCollection
          .doc(groupId)
          .collection(AppConstants.CHATS_COLLECTION)
          .doc(timeStamp.millisecondsSinceEpoch.toString())
          .set(data);
    } catch (error) {
      print('****************** DB addNewMessage error **********************');
      print(error);
      throw error;
    }
  }

  Future<QuerySnapshot<Map<String,dynamic>>> getChatItemData(String groupId, [int limit = 20]) {
    try {
      return _messagesCollection
          .doc(groupId)
          .collection(AppConstants.CHATS_COLLECTION)
          .orderBy('timeStamp', descending: true)
          .limit(limit)
          .get();
    } catch (error) {
      print(
          '****************** DB getChatItemData error **********************');
      throw error;
    }
  }

// USER INFO

  Stream<QuerySnapshot> getContactsStream() {
    return _usersCollection.snapshots();
  }

  Stream<DocumentSnapshot> getUserContactsStream(String uid) {
    try{
      print("obtaining user contact =====");
      var snapshots =_usersCollection.doc(uid).snapshots();
      print("obtained user contact =====");
      return snapshots;

    }on NoSuchMethodError catch (ne) {
      print(CustomTrace(StackTrace.current, message: ne.toString()));
      throw ne;
    } on Exception catch (error) {
      print("error caught");
      throw error;
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e.toString()));
      throw e;
    }

  }

  Future<DocumentSnapshot> getUser(String id) {
    try {
      print("getting user db ******");
      return _usersCollection.doc(id).get();
    } on NoSuchMethodError catch (ne) {
      if (kDebugMode) {
        print(CustomTrace(StackTrace.current, message: ne.toString()));
      }
      throw ne;
    } on Exception catch (error) {
      print("error caught");
      throw error;
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e.toString()));
      throw e;
    }
  }

  void addNewUser(
      String userId, String imageUrl, String username, String email) {
    try {
      _usersCollection.doc(userId).set({
        'id': userId,
        'imageUrl': imageUrl,
        'username': username,
        'email': email,
        'contacts': [],
      });
    } catch (error) {
      print('****************** DB addNewUser error **********************');
      print(error);
      throw error;
    }
  }

  Future<DocumentSnapshot> getUserDocRef(String userId) async {
    try {
      return _usersCollection.doc(userId).get();
    } catch (error) {
      print('****************** DB getUserDocRef error **********************');
      print(error);
      throw error;
    }
  }

  void updateUserInfo(String userId, Map<String, dynamic> data) async {
    try {
      _usersCollection.doc(userId).set(data, SetOptions( merge: true ) );
    } catch (error) {
      print(
          '****************** DB updateUserInfo error **********************');
      print(error);
      throw error;
    }
  }

  void updateContacts(String userId, dynamic contacts) {
    try {
      _usersCollection
          .doc(userId)
          .set({'contacts': contacts}, SetOptions(  merge: true));
    } catch (error) {
      print(
          '****************** DB updateContacts error **********************');
      print(error);
      throw error;
    }
  }

  Future<DocumentSnapshot> addToPeerContacts(
      String peerId, String newContact) async {
    DocumentReference doc;
    DocumentSnapshot<Map<String,dynamic>> docSnapshot;

    try {
      doc = _usersCollection.doc(peerId);
      docSnapshot = await doc.get() as DocumentSnapshot<Map<String,dynamic>>;

      var peerContacts = [];

      docSnapshot.data()!['contacts'].forEach((elem) => peerContacts.add(elem));
      peerContacts.add(newContact);

      FirebaseFirestore.instance.runTransaction((transaction) async {
        final freshDoc = await transaction.get(doc);
        transaction.update(freshDoc.reference, {'contacts': peerContacts});
      });

      // doc.setData({'contacts': peerContacts}, merge: true);
    } catch (error) {
      print(
          '****************** DB addToPeerContacts error **********************');
      print(error);
      throw error;
    }

    return docSnapshot;
  }

  void updateMessageField(dynamic snapshot, String field, dynamic value) {
    try {
      FirebaseFirestore.instance.runTransaction((transaction) async {
        // DocumentSnapshot freshDoc = await transaction.get(snapshot.reference);
        transaction.update(snapshot.reference, {'$field': value});
      });
    } catch (error) {
      print(
          '****************** DB updateMessageField error **********************');
      print(error);
      throw error;
    }
  }

  Stream<QuerySnapshot> getSnapshotsAfter(
      String groupChatId, DocumentSnapshot lastSnapshot) {
    try {
      return _messagesCollection
          .doc(groupChatId)
          .collection(AppConstants.CHATS_COLLECTION)
          .orderBy('timeStamp')
          .startAfterDocument(lastSnapshot)
          .snapshots();
    } catch (error) {
      print(
          '****************** DB getSnapshotsAfter error **********************');
      print(error);
      throw error;
    }
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getSnapshotsWithLimit(String groupChatId,
      [int limit = 10]) {
    try {
      return _messagesCollection
          .doc(groupChatId)
          .collection(AppConstants.CHATS_COLLECTION)
          .limit(limit)
          .orderBy('timeStamp', descending: true)
          .snapshots();
    } catch (error) {
      print(
          '****************** DB getSnapshotsWithLimit error **********************');
      print(error);
      throw error;
    }
  }

  void addMediaUrl(String groupId, String url, Message mediaMsg) {
    try {
      _messagesCollection
          .doc(groupId)
          .collection(AppConstants.MEDIA_COLLECTION)
          .doc(mediaMsg.timeStamp)
          .set(MediaModel.fromMsgToMap(mediaMsg));
    } catch (error) {
      print('****************** DB addMediaUrl error **********************');
      print(error);
      throw error;
    }
  }


}