import 'dart:convert';
import 'dart:io';

import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

import '../helpers/custom_trace.dart';
import '../helpers/helper.dart';
import '../models/credit_card.dart';
import '../models/consultation.dart';
import '../models/consultation_status.dart';
import '../models/payment.dart';
import '../models/user.dart';
import '../repository/user_repository.dart' as user_repo;

Future<Stream<Consultation>> getConsultations() async {
  User _user = user_repo.currentUser.value;
  if (_user.apiToken == null) {
    return  Stream.value(Consultation());
  }
  final String _apiToken = 'api_token=${_user.apiToken}&';
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}consultations?${_apiToken}with=user;productConsultations;productConsultations.product;productConsultations.options;consultationStatus;payment&search=user.id:${_user.id}&searchFields=user.id:=&orderBy=id&sortedBy=desc';
  try {
    final client = http.Client();
    final streamedRest = await client.send(http.Request('get', Uri.parse(url)));

    return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data as Map<String, dynamic>)).expand((data) => (data as List)).map((data) {
      return Consultation.fromJSON(data);
    });
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: url).toString());
    return  Stream.value(Consultation.fromJSON({}));
  }
}

Future<Stream<Consultation>> getConsultation(consultationId) async {
  User _user = user_repo.currentUser.value;
  if (_user.apiToken == null) {
    return  Stream.value(Consultation());
  }
  final String _apiToken = 'api_token=${_user.apiToken}&';
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}consultations/$consultationId?${_apiToken}with=user;productConsultations;productConsultations.product;productConsultations.options;consultationStatus;billingAddress;payment';
  final client = new http.Client();
  final streamedRest = await client.send(http.Request('get', Uri.parse(url)));

  return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data as Map<String, dynamic>)).map((data) {
    return Consultation.fromJSON(data);
  });
}

Future<Stream<Consultation>> getHealerConsultations() async {
  User _user = user_repo.currentUser.value;
  if (_user.apiToken == null) {
    return Stream.value(Consultation());
  }
  final String _apiToken = 'api_token=${_user.apiToken}&';
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}consultations?${_apiToken}user_type=healer&with=user;productConsultations;productConsultations.product;productConsultations.options;consultationStatus;payment';
  try {
    final client =  http.Client();
    final streamedRest = await client.send(http.Request('get', Uri.parse(url)));

    return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data as Map<String, dynamic>)).expand((data) => (data as List)).map((data) {
      return Consultation.fromJSON(data);
    });
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: url).toString());
    return  Stream.value(new Consultation.fromJSON({}));
  }
}

Future<Stream<Consultation>> getRecentConsultations() async {
  User _user = user_repo.currentUser.value;
  if (_user.apiToken == null) {
    return Stream.value(Consultation());
  }
  final String _apiToken = 'api_token=${_user.apiToken}&';
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}consultations?${_apiToken}with=user;productConsultations;productConsultations.product;productConsultations.options;consultationStatus;payment&search=user.id:${_user.id}&searchFields=user.id:=&orderBy=updated_at&sortedBy=desc&limit=3';

  final client =  http.Client();
  final streamedRest = await client.send(http.Request('get', Uri.parse(url)));

  return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data as Map<String, dynamic>)).expand((data) => (data as List)).map((data) {
    return Consultation.fromJSON(data);
  });
}

Future<Stream<ConsultationStatus>> getConsultationStatus() async {
  User _user = user_repo.currentUser.value;
  if (_user.apiToken == null) {
    return  Stream.value(ConsultationStatus());
  }
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}consultation_statuses?$_apiToken';

  final client =  http.Client();
  final streamedRest = await client.send(http.Request('get', Uri.parse(url)));

  return streamedRest.stream.transform(utf8.decoder).transform(json.decoder).map((data) => Helper.getData(data as Map<String, dynamic>)).expand((data) => (data as List)).map((data) {
    return ConsultationStatus.fromJSON(data);
  });
}

Future<Consultation> addUserConsultation(Consultation consultation, Payment payment) async {
  User user = user_repo.currentUser.value;
  if (user.apiToken == null) {
    return  Consultation();
  }
  CreditCard _creditCard = getCreditCard();
  consultation.id = "";
  consultation.productConsultations.first.id="";
  consultation.consultationStatus.id="1";

  consultation.user = user;
  consultation.payment = getPayment(payment) ;
  final String _apiToken = 'api_token=${user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}consultations?$_apiToken';
  Uri uri = Uri.parse(url);
  final client =  http.Client();
  Map params = consultation.toMap();
  print(json.encode(consultation.toMap()));
 /***params.addAll(_creditCard.toMap()); Remove for strategic reason***/
  print(json.encode(params));
  final response = await client.post(
    uri,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(params),
  );
  return Consultation.fromJSON(json.decode(response.body)['data']);
}

Future<Consultation> cancelConsultation(Consultation consultation) async {
  print(consultation.toMap());
  User _user = user_repo.currentUser.value;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}consultations/${consultation.id}?$_apiToken';
  Uri uri = Uri.parse(url);
  final client =  http.Client();
  final response = await client.put(
    uri,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(consultation.cancelMap()),
  );
  if (response.statusCode == 200) {
    return Consultation.fromJSON(json.decode(response.body)['data']);
  } else {
    throw  Exception(response.body);
  }
}

Payment getPayment(Payment payment){
  payment.id ="";
  payment.status = "Paid";
  return payment;
}

CreditCard getCreditCard(){
  var creds =  CreditCard();
  creds.id ="";
  creds.number= "4545880010078839";
  creds.expMonth="03";
  creds.expYear="23";
  creds.cvc ="883";
  return creds;
}