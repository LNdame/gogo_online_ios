import 'package:flutter/material.dart';
import 'package:gogo_online_ios/main.dart';
import 'package:gogo_online_ios/route_constants.dart';
import 'package:gogo_online_ios/src/helpers/app_constants.dart';
import 'package:gogo_online_ios/src/models/route_argument.dart';
import 'package:gogo_online_ios/src/models/user.dart';
import 'package:gogo_online_ios/src/pages/consultation_success.dart';
import 'package:gogo_online_ios/src/pages/consultation_summary.dart';
import 'package:gogo_online_ios/src/pages/details.dart';
import 'package:gogo_online_ios/src/pages/healer/healer_pages.dart';
import 'package:gogo_online_ios/src/pages/healer/healer_signup.dart';
import 'package:gogo_online_ios/src/pages/healer/healer_signup_success.dart';
import 'package:gogo_online_ios/src/pages/help.dart';
import 'package:gogo_online_ios/src/pages/languages.dart';
import 'package:gogo_online_ios/src/pages/login.dart';
import 'package:gogo_online_ios/src/pages/netcash_payment.dart';
import 'package:gogo_online_ios/src/pages/onboarding.dart';
import 'package:gogo_online_ios/src/pages/pages.dart';
import 'package:gogo_online_ios/src/pages/profile.dart';
import 'package:gogo_online_ios/src/pages/scheduler.dart';
import 'package:gogo_online_ios/src/pages/settings.dart';
import 'package:gogo_online_ios/src/pages/signup.dart';
import 'package:gogo_online_ios/src/pages/splash_screen.dart';
import 'package:gogo_online_ios/src/pages/tracking.dart';
import 'package:gogo_online_ios/src/pages/waiting_room.dart';
import 'package:gogo_online_ios/src/controllers/cart_controller.dart';
import 'package:gogo_online_ios/src/models/cart.dart';
import 'package:gogo_online_ios/src/utils/picture_utils/set_photo_screen.dart';
import 'src/pages/product.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case splashRoute:
        return MaterialPageRoute(builder: (_) => const SplashScreen());
      case onboardingRoute:
        return MaterialPageRoute(builder: (_) => const OnBoardingPage());

      case loginRoute:
        return MaterialPageRoute(builder: (_) => const LoginWidget());
      case signUpRoute:
        return MaterialPageRoute(builder: (_) => const SignUpWidget());
      case pagesRoute:
        return MaterialPageRoute(builder: (_) => PagesWidget(currentTab: args));
      case detailsRoute:
        return MaterialPageRoute(builder: (_) => DetailsWidget(routeArgument: args as RouteArgument));
      case productRoute:
        return MaterialPageRoute(builder: (_) => ProductWidget(routeArgument: args as RouteArgument));
      case cartRoute:
        return MaterialPageRoute(builder: (_) => WaitingRoomWidget(routeArgument: args as RouteArgument));
      case consultationSummaryRoute:
        return MaterialPageRoute(builder: (_) => ConsultationSummaryWidget());
      case payOnPickupRoute:
        return MaterialPageRoute(
            builder: (_) => ConsultationSuccessWidget(routeArgument: RouteArgument(param: AppConstants.paidNetcash)));
      case healerPagesRoute:
        return MaterialPageRoute(builder: (_) => HealerPagesWidget(currentTab: args));
      case healerRegisterRoute:
        return MaterialPageRoute(builder: (_) => HealerRegistrationWidget());
      case healerRegisterSuccessRoute:
        return MaterialPageRoute(builder: (_) => HealerSignUpSuccessWidget());
      case netCashRoute:
        return MaterialPageRoute(
            builder: (_) => NetCashPaymentWidget(
                  routeArgument: args as RouteArgument,
                  carts: [],
                  total: 0,
                ));
      case consultationSuccessRoute:
        return MaterialPageRoute(builder: (_) => ConsultationSuccessWidget(routeArgument: args as RouteArgument));
      case languagesRoute:
        return MaterialPageRoute(builder: (_) => LanguagesWidget());
      case helpRoute:
        return MaterialPageRoute(builder: (_) => HelpWidget());
      case settingsRoute:
        return MaterialPageRoute(builder: (_) => SettingsWidget());
      case schedulerRoute:
        var cart = settings.arguments as Cart;
        var con = settings.arguments as CartController;
        return MaterialPageRoute(builder: (_) => SchedulerWidget(cart: cart, con: con));
      case trackingRoute:
        return MaterialPageRoute(builder: (_) => TrackingWidget(routeArgument: args as RouteArgument));
      case profileRoute:
        return MaterialPageRoute(builder: (_) => ProfileWidget());
      case setPhotoScreenRoute:
        var user = settings.arguments as User;
        return MaterialPageRoute(builder: (_) => SetPhotoScreen( user: user));
      default:
        // If there is no such named route in the switch statement, e.g. /third
        return MaterialPageRoute(builder: (_) => Scaffold(body: SafeArea(child: Text('Route Error'))));
    }
  }
}
